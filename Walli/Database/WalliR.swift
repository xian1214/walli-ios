//
//  Record.swift
//  SqliteIntegration
//
//  Created by Ayush Gupta on 1/20/19.
//  Copyright © 2019 Ayush Gupta. All rights reserved.
//
import Foundation
import CoreBluetooth
class WalliR {
    enum CardStatus{
        case CARD_STATUS_NORMAL
        case CARD_STATUS_OUT
    }
    static let RANGE_NEAR = 0
    static let RANGE_MODERATE = 1
    static let RANGE_FAR = 2
    
    static let SEPARATION_ALARM_OFF = 0
    static let SEPARATION_ALARM_MUTE = 1
    static let SEPARATION_ALARM_ON = 2
        
    var devicename: String
    var macaddress: String
    var devicetype: Int
    var latitude: Double
    var longitude: Double
    var address: String
    
    var rangemode: Int
    var cardnotificationdelay: Int
    var trackertype: Int
    var losttime: Int
    var alarmmode: Int
    var pos: Int
    var battery: Int
    var isRinging: Bool
    var cardstatus: CardStatus
    var peripheral: CBPeripheral? = nil
    var trackerAssets = ["ic_tracker_key", "ic_tracker_remote", "ic_tracker_luggage", "ic_tracker_pet", "ic_tracker_bagpack", "ic_tracker_camera", "ic_tracker_car", "ic_tracker_other"]
    init(devicetype: Int) {
        self.devicename = ""
        self.macaddress = ""
        self.devicetype = devicetype
        self.latitude = 0
        self.longitude = 0
        self.address = ""
        self.rangemode = 0
        self.cardnotificationdelay = 10
        self.trackertype = 0
        self.losttime = 0
        self.alarmmode = 0
        self.pos = 0
        self.battery = 0
        self.isRinging = false
        self.cardstatus = .CARD_STATUS_NORMAL
    }
    
    func setMacAddress(macAddr: String) {
        self.macaddress = macAddr
    }
    func getMacAddress() -> String {
        return self.macaddress
    }
    func setDeviceName(devicename: String) {
        self.devicename = devicename
    }

    func setDeviceType(devicetype: Int) {
        self.devicetype = devicetype
    }

    func setTrackType(tracktype: Int) {
        self.trackertype = tracktype
    }
    func getTrackImage() -> String {
        return trackerAssets[self.trackertype]
    }
    func setLostLatitude(latitude: Double) {
        self.latitude = latitude
    }
    
    func setLostLongitude(longitude: Double) {
        self.longitude = longitude
    }
    
    func setLostAddress(address: String){
        self.address = address
    }

    func setLostTime(losttime: Int) {
        self.losttime = losttime
    }

    func setRangeMode(rangemode: Int) {
        self.rangemode = rangemode
    }

    func setAlarmMode(alarmmode: Int) {
        self.alarmmode = alarmmode
    }

    func setCardNotificationDelay(cardnotificationdelay: Int) {
        self.cardnotificationdelay = cardnotificationdelay
    }

    func setPosition(pos: Int) {
        self.pos = pos
    }
    func isWalliConnected() -> Bool{
        if self.peripheral != nil && self.peripheral?.state == CBPeripheralState.connected{
            return true
        }
        return false
    }
}
