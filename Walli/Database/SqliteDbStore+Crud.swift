//
//  SqliteDbStore+CrudOperations.swift
//  SqliteIntegration
//
//  Created by Ayush Gupta on 1/20/19.
//  Copyright © 2019 Ayush Gupta. All rights reserved.
//
import Foundation
import SQLite3

extension SqliteDbStore {
    static let GEN_DEVICE_MACADDR_FIELD = 1
    static let GEN_DEVICE_NAME_FIELD = 2
    static let GEN_DEVICE_TYPE_FIELD = 3
    static let GEN_DEVICE_TRACKTYPE_FIELD = 4
    static let GEN_DEVICE_LOSTLATITUDE_FIELD = 5
    static let GEN_DEVICE_LOSTLONGITUDE_FIELD = 6
    static let GEN_DEVICE_LOSTADDRESS_FIELD = 7
    static let GEN_DEVICE_LOSTTIME_FIELD = 8
    static let GEN_DEVICE_RANGEMODE_FIELD = 9
    static let GEN_DEVICE_ALARMMODE_FIELD = 10
    static let GEN_DEVICE_CARDNOTIFICATIONTIMEDELAY_FIELD = 11
    
    //"INSERT INTO WalliRTable (Name, MacAddress, Designation) VALUES (?,?,?)"
    func insert(record: WalliR) {
        // ensure statements are created on first usage if nil
        guard self.prepareInsertEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.insertEntryStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting macaddress in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_MACADDR_FIELD), (record.macaddress as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        //Inserting name in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_NAME_FIELD), (record.devicename as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        //Inserting devicetype in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_TYPE_FIELD), Int32(record.devicetype)) != SQLITE_OK{
            logDbErr("sqlite_bind_int(insertEntryStmt")
            return
        }
        //Inserting tracktype in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_TRACKTYPE_FIELD), Int32(record.trackertype)) != SQLITE_OK{
            logDbErr("sqlite_bind_int(insertEntryStmt")
            return
        }
        //Inserting latitude in insertEntryStmt prepared statement
        if sqlite3_bind_double(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTLATITUDE_FIELD), record.latitude) != SQLITE_OK{
            logDbErr("sqlite_bind_int(insertEntryStmt")
            return
        }
        //Inserting longitude in insertEntryStmt prepared statement
        if sqlite3_bind_double(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTLONGITUDE_FIELD), record.longitude) != SQLITE_OK{
            logDbErr("sqlite_bind_int(insertEntryStmt")
            return
        }
        //Inserting address in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTADDRESS_FIELD), (record.address as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        //Inserting losttime in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTTIME_FIELD), Int32(record.losttime)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(insertEntryStmt)")
            return
        }
        //Inserting rangemode in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_RANGEMODE_FIELD), Int32(record.rangemode)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(insertEntryStmt)")
            return
        }
        //Inserting alarmmode in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_ALARMMODE_FIELD), Int32(record.alarmmode)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(insertEntryStmt)")
            return
        }
        //Inserting cardnotificationdelay in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_CARDNOTIFICATIONTIMEDELAY_FIELD), Int32(record.cardnotificationdelay)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(insertEntryStmt)")
            return
        }
        //executing the query to insert values
        let r = sqlite3_step(self.insertEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(insertEntryStmt) \(r)")
            return
        }
    }
    
    //"SELECT * FROM WalliRTable WHERE MacAddress = ? LIMIT 1"
    func readAll() throws -> Dictionary<String, WalliR> {
        // ensure statements are created on first usage if nil
        guard self.prepareReadAllEntryStmt() == SQLITE_OK else { throw SqliteError(message: "Error in prepareReadEntryStmt") }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.readEntryStmt)
        }
        
        var dict = Dictionary<String, WalliR>()
        var pos = 0

        while (sqlite3_step(self.readEntryStmt) == SQLITE_ROW){
            //Inserting MacAddress in readEntryStmt prepared statement
            pos += 1
            let walliR = WalliR(devicetype: 0)
            walliR.setMacAddress(macAddr: String(cString: sqlite3_column_text(self.readEntryStmt, 0)))
            walliR.setDeviceName(devicename: String(cString: sqlite3_column_text(self.readEntryStmt, 1)))
            walliR.setDeviceType(devicetype: Int(sqlite3_column_int(self.readEntryStmt, 2)))
            walliR.setTrackType(tracktype: Int(sqlite3_column_int(self.readEntryStmt, 3)))
            walliR.setLostLatitude(latitude: sqlite3_column_double(self.readEntryStmt, 4))
            walliR.setLostLongitude(longitude: sqlite3_column_double(self.readEntryStmt, 5))
            walliR.setLostAddress(address: String(cString: sqlite3_column_text(self.readEntryStmt, 6)))
            walliR.setLostTime(losttime: Int(sqlite3_column_int(self.readEntryStmt, 7)))
            walliR.setRangeMode(rangemode: Int(sqlite3_column_int(self.readEntryStmt, 8)))
            walliR.setAlarmMode(alarmmode: Int(sqlite3_column_int(self.readEntryStmt, 9)))
            walliR.setCardNotificationDelay(cardnotificationdelay: Int(sqlite3_column_int(self.readEntryStmt, 10)))
            walliR.setPosition(pos: pos)
            dict[walliR.getMacAddress()] = walliR
        }
        return dict
    }
    
    //"UPDATE WalliRTable SET Name = ?, Designation = ? WHERE MacAddress = ?"
    func update(record: WalliR) {
        // ensure statements are created on first usage if nil
        guard self.prepareUpdateEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.updateEntryStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Updaing macaddress in updateEntryStmt prepared statement
        if sqlite3_bind_text(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_MACADDR_FIELD), (record.macaddress as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(updateEntryStmt)")
            return
        }
        //Updaing name in updateEntryStmt prepared statement
        if sqlite3_bind_text(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_NAME_FIELD), (record.devicename as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(updateEntryStmt)")
            return
        }
        //Updaing devicetype in updateEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_TYPE_FIELD), Int32(record.devicetype)) != SQLITE_OK{
            logDbErr("sqlite_bind_int(updateEntryStmt")
            return
        }
        //Updaing tracktype in updateEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_TRACKTYPE_FIELD), Int32(record.trackertype)) != SQLITE_OK{
            logDbErr("sqlite_bind_int(updateEntryStmt")
            return
        }
        //Updaing latitude in updateEntryStmt prepared statement
        if sqlite3_bind_double(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTLATITUDE_FIELD), record.latitude) != SQLITE_OK{
            logDbErr("sqlite_bind_int(updateEntryStmt")
            return
        }
        //Updaing longitude in updateEntryStmt prepared statement
        if sqlite3_bind_double(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTLONGITUDE_FIELD), record.longitude) != SQLITE_OK{
            logDbErr("sqlite_bind_int(updateEntryStmt")
            return
        }
        //Updaing address in updateEntryStmt prepared statement
        if sqlite3_bind_text(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTADDRESS_FIELD), (record.address as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(updateEntryStmt)")
            return
        }
        //Updaing losttime in updateEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_LOSTTIME_FIELD), Int32(record.losttime)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(updateEntryStmt)")
            return
        }
        //Updaing rangemode in updateEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_RANGEMODE_FIELD), Int32(record.rangemode)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(updateEntryStmt)")
            return
        }
        //Updaing alarmmode in updateEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_ALARMMODE_FIELD), Int32(record.alarmmode)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(updateEntryStmt)")
            return
        }
        //Updaing cardnotificationdelay in updateEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, Int32(SqliteDbStore.GEN_DEVICE_CARDNOTIFICATIONTIMEDELAY_FIELD), Int32(record.cardnotificationdelay)) != SQLITE_OK {
            logDbErr("sqlite_bind_int(updateEntryStmt)")
            return
        }
        //Updaing all by Where Statement in updateEntryStmt prepared statement
        if sqlite3_bind_text(self.updateEntryStmt, 12, (record.macaddress as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(updateEntryStmt)")
            return
        }
        //executing the query to update values
        let r = sqlite3_step(self.updateEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(updateEntryStmt) \(r)")
            return
        }
    }
    
    //"DELETE FROM WalliRTable WHERE macAddress = ?"
    func delete(MacAddress: String) {
        // ensure statements are created on first usage if nil
        guard self.prepareDeleteEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.deleteEntryStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting name in deleteEntryStmt prepared statement
        if sqlite3_bind_text(self.deleteEntryStmt, 1, (MacAddress as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteEntryStmt)")
            return
        }
        
        //executing the query to delete row
        let r = sqlite3_step(self.deleteEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteEntryStmt) \(r)")
            return
        }
    }
    
    // INSERT/CREATE operation prepared statement
    func prepareInsertEntryStmt() -> Int32 {
        guard insertEntryStmt == nil else { return SQLITE_OK }
        let sql = "INSERT INTO WalliRTable (MacAddress, Name, Type, TrackType, Latitude, Longitude, LocAddress, LostTime, RangeMode, AlarmMode, CardNotificationDelay) VALUES (?,?,?,?,?,?,?,?,?,?,?)"
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &insertEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare insertEntryStmt")
        }
        return r
    }
    
    // READ operation prepared statement
    func prepareReadAllEntryStmt() -> Int32 {
        guard readEntryStmt == nil else { return SQLITE_OK }
        let sql = "SELECT * FROM WalliRTable"
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &readEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare readEntryStmt")
        }
        return r
    }
    
    // UPDATE operation prepared statement
    func prepareUpdateEntryStmt() -> Int32 {
        guard updateEntryStmt == nil else { return SQLITE_OK }
        let sql = "UPDATE WalliRTable SET MacAddress = ?, Name = ?, Type = ?, TrackType = ?, Latitude = ?, Longitude = ?, LocAddress = ?, LostTime = ?, RangeMode = ?, AlarmMode = ?, CardNotificationDelay = ? WHERE MacAddress = ?"
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &updateEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare updateEntryStmt")
        }
        return r
    }
    
    // DELETE operation prepared statement
    func prepareDeleteEntryStmt() -> Int32 {
        guard deleteEntryStmt == nil else { return SQLITE_OK }
        let sql = "DELETE FROM WalliRTable WHERE MacAddress = ?"
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &deleteEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare deleteEntryStmt")
        }
        return r
    }
}
