//
//  UILabel+Spacing.swift
//  Walli
//
//  Created by devstar on 10/11/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    public func setLineHeight(lineHeight: CGFloat) {
        let text = self.text
        if let text = text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            
            style.alignment = .center
            style.lineSpacing = lineHeight
            attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, attributeString.length))
            self.attributedText = attributeString
        }
    }

}
