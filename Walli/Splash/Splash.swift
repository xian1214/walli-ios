//
//  Splash.swift
//  Walli
//
//  Created by Xian Huang on 1/21/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class Splash: UIViewController {

    @IBOutlet weak var imgLogoGif: UIImageView!
    @IBOutlet weak var imgBackgroundGif: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if PrefsManager.shared.getDeviceCount() > 0{
            gotoPhoneAuth()
        }
        else{
            let logoGif = UIImage.gif(asset: "w")
            self.imgLogoGif.animationImages = logoGif?.images
            self.imgLogoGif.image = logoGif?.images![(logoGif?.images!.count)!-1]
            self.imgLogoGif.animationRepeatCount = 1
            self.imgLogoGif.startAnimating()
            //imgBackgroundGif.loadGif(asset: "splash_back")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        // Do any additional setup after loading the view.
        updateProgressRatio()
    }

    func updateProgressRatio() {
        DispatchQueue.main.asyncAfter(deadline: .now()+4) {
            self.imgLogoGif.stopAnimating()
            self.gotoPhoneAuth()
        }
    }

    func gotoPhoneAuth() {
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: true, completion: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()

        UIApplication.shared.windows.first?.rootViewController = vc
    }

}
