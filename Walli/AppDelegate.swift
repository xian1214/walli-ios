//
//  AppDelegate.swift
//  Walli
//
//  Created by Xian Huang on 1/20/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import BackgroundTasks
import CoreBluetooth
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    var cvc: ContentMainViewController? = nil
    var svc: ScanDeviceViewController? = nil
    var lvc: LostCodeViewController? = nil
    var pvc: FourthTutorialViewController? = nil
    var cpvc: CheckPermissionViewController? = nil
    var audioPlayer: AVAudioPlayer? = nil
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        UNUserNotificationCenter.current().delegate = self
        //self.requestNotificationPermission()

        
        if let options = launchOptions {
            /*
            if var remoteNotification: NSDictionary = options[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary {
                // Awake from remote notification
                // No further logic here, will be handled by application didReceiveRemoteNotification fetchCompletionHandler
                NotificationUtil.sendNotification(title: "Walli", body: "Awake from remote notification")
            }
             */
            if var centralManagerIdentifiers: NSDictionary = (options[UIApplication.LaunchOptionsKey.bluetoothCentrals] as? NSDictionary){
                if centralManagerIdentifiers.count>0{
                    if PrefsManager.shared.getDeviceCount() > 0{
                        // bluetooth manager
                        BLEManager.getSharedBLEManager().initCentralManager()
                    }
                }
            }
 

        }
        if PrefsManager.shared.getDeviceCount() > 0{
            PrefsManager.shared.setEmergencySMS(value: 0)
            // bluetooth manager
            BLEManager.getSharedBLEManager().initCentralManager()
            // location manager
            LocationManager.getSharedManager().initLocationManager()
        }

        return true
    }
    func requestNotificationPermission() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted{
                print("Permission granted for local notifications.")
            }
            else{
                print("Permission denined for local notifications.")
//                self.getNotificationSettgins()
//                self.openAppOrSystemSettingsAlert(title: "Notification permission is currently disabled for the application. Enable notification from the application settings", message: "")
            }
        }

    }
    func getNotificationSettgins(){
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
              UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    /*
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
    }
 */
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        print("Application will resign active")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Application did enter background")

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        print("Application will enter foreground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("Application did become active")
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("Application will terminate")
        LocationManager.getSharedManager().saveLastLocation()
    }
    func playBackgroundMusic(filename: String){
        let path = Bundle.main.path(forResource: filename, ofType: nil)
        let url = URL(fileURLWithPath: path!)
        do{
            if(self.audioPlayer != nil && self.audioPlayer!.isPlaying){
                self.audioPlayer?.stop()
            }
            
            self.audioPlayer = try AVAudioPlayer(contentsOf: url)
            do {
                MPVolumeView.setVolume(1.0)
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [.mixWithOthers, .allowAirPlay])
                print("Playback OK")
                try AVAudioSession.sharedInstance().setActive(true)
                print("Session is Active")
            } catch {
                print(error)
            }

            self.audioPlayer?.play()
            
        } catch{
            
        }
    }
    func stopPlay(){
        print("Audio Player Stop 1...")
        if(self.audioPlayer != nil ){
            print("Audio Player Stop 2...")
            self.audioPlayer?.stop()
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("action identifier \(response.actionIdentifier)")
        completionHandler()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
    }
    //MARK: Regiater BackGround Tasks
    func registerBackgroundTasks() {
        
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.mywalli.blecheck", using: nil) { task in
            //This task is cast with processing request (BGProcessingTask)
            self.handleImageFetcherTask(task: task as! BGProcessingTask)
        }
        
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "com.mywalli.apprefresh", using: nil) { task in
            //This task is cast with processing request (BGAppRefreshTask)
            self.handleAppRefreshTask(task: task as! BGAppRefreshTask)
        }
    }
    func cancelAllPandingBGTask() {
        BGTaskScheduler.shared.cancelAllTaskRequests()
    }
    
    func scheduleImageFetcher() {
        let request = BGProcessingTaskRequest(identifier: "com.mywalli.blecheck")
        request.requiresNetworkConnectivity = false // Need to true if your task need to network process. Defaults to false.
        request.requiresExternalPower = false
        
        request.earliestBeginDate = Date(timeIntervalSinceNow: 1 * 10) // Featch Image Count after 1 minute.
        //Note :: EarliestBeginDate should not be set to too far into the future.
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Could not schedule image featch: \(error)")
        }
    }
    
    func scheduleAppRefresh() {
        let request = BGAppRefreshTaskRequest(identifier: "com.mywalli.apprefresh")
        request.earliestBeginDate = Date(timeIntervalSinceNow: 2 * 10) // App Refresh after 2 minute.
        //Note :: EarliestBeginDate should not be set to too far into the future.
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            print("Could not schedule app refresh: \(error)")
        }
    }
    
    func handleAppRefreshTask(task: BGAppRefreshTask) {
        //Todo Work
        scheduleAppRefresh()
        /*
         //AppRefresh Process
         */
        print("------------HandleAppRefreshTask-------------")
        task.expirationHandler = {
            //This Block call by System
            //Canle your all tak's & queues
            print("Expiration Handler")
        }
        //
        task.setTaskCompleted(success: true)
    }
    
    func handleImageFetcherTask(task: BGProcessingTask) {
        scheduleImageFetcher() // Recall
        
        print("Handle Image Fetcher Task")
        //Todo Work
        task.expirationHandler = {
            //This Block call by System
            //Canle your all tak's & queues
            print("Expiration Handler")
        }
        
        //Get & Set New Data
        print("Get and Set New Data")
        
        //
        task.setTaskCompleted(success: true)
        
    }
}

extension MPVolumeView {
  static func setVolume(_ volume: Float) {
    let volumeView = MPVolumeView()
    let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider

    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
      slider?.value = volume
    }
  }
}
