//
//  Constant.swift
//  Walli
//
//  Created by Xian Huang on 1/29/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation

class Constant{
    static let PRODUCT_WALLI = 0
    static let PRODUCT_WALLI_TRAVEL = 1
    static let PRODUCT_WALLI_SLIM = 2
    static let PRODUCT_KEYFINDER = 3
    static let PRODUCT_TRACKER = 4
    
    static let PREFIX_WALLI = "WALLI"
    static let PREFIX_SLIM = "SLIM"
    static let PREFIX_TRAVEL = "WALLI"
    static let PREFIX_KEYFINDER = "KF"
    static let PREFIX_TRACKER = "iTrack"
    
    
    static let TIP_WALLICODE = "This code is found on the\n   backside of the box"
    static let TIP_RANGE = "Set the distance range for when\n   the app should notify you that you\n   have left your Walli behind"
    static let TIP_CARDNOTIFY = "Set the time after which you\n   want to get notified when a\n   card is out of Walli pocket"
    static let TIP_SEPARATION_ALARM = "Customize Separation Alarm\n   ON = You will get an audible\n   (sound) notification\n   MUTE = You will get a visual/text\n   notification\n   OFF = You will not receive any\n   notification"
    static let TIP_EMERGENCY = "The Personal Safety Button feature allows you\n to send an emergency SMS text message \n(with your current GPS location) \nto two of your emergency contacts. \nLong press the button TWICE to use this feature.\nIt is very important that you set your \nFULL NAME so that your emergency\n contacts can recognize you, as the text message\n will be sent from a random number on our server\n and will not be sent from your phone number"
    
    static let SOUND_WALLI_CONNECT = "WalliConnect"
    static let SOUND_WALLI_DISCONNECT = "WalliDisconnect"
    static let SOUND_CARD_OUT = "CardOut"
    static let SOUND_FIND_PHONE = "FindPhone"
    static let SOUND_BLUETOOTH_OFF = "BluetoothOff"
    static let SOUND_NONE = "None"
}
