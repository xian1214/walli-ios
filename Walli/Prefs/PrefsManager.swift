//
//  PrefsManager.swift
//  Walli
//
//  Created by Xian Huang on 1/28/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation
import CoreBluetooth
import CoreLocation
//private var sharedPrefsManager: PrefsManager? = nil

class PrefsManager{
    static let KEY_DEVICETYPE = "KeyDeviceType"
    static let KEY_DEVICENAME = "KeyDeviceName"
    static let KEY_WIFISILENTZONEMODE = "KeyWifiSilentZoneMode"
    static let KEY_BACKPACKETMODE = "KeyBackPacketMode"
    static let KEY_HOMEWIFISSID = "KeyHomeWifiSSID"
    static let KEY_USEDWIFILIST = "KeyWifiList"
    static let KEY_EMERGENCYCONTACT = "KeyEmergencyContact"
    static let KEY_CONTACTMYNAME = "KeyContactMyName"
    static let KEY_CONTACTNAME1 = "KeyContactName1"
    static let KEY_CONTACTNAME2 = "KeyContactName2"
    static let KEY_CONTACTNUMBER1 = "KeyContactNumber1"
    static let KEY_CONTACTNUMBER2 = "KeyContactNumber2"
    static let KEY_EMERGENCY_SMS = "KeyEmergencySMS"
    static let shared: PrefsManager = {
        let pM = PrefsManager()
        return pM
    }()

    var mSqliteDB: SqliteDbStore
    var mDeviceMap: Dictionary<String, WalliR>? = nil
    var mLastLocation: CLLocation!
/*
    static func getSharedBLEManager () -> PrefsManager {
        if sharedPrefsManager == nil {
            sharedPrefsManager = PrefsManager()
        }
        return sharedPrefsManager!
    }
*/
    init(){
        mSqliteDB = SqliteDbStore()
    }
    func setDeviceType(deviceType: Int){
        UserDefaults.standard.set(deviceType, forKey: PrefsManager.KEY_DEVICETYPE)
    }
    func getDeviceType() -> Int{
        return UserDefaults.standard.integer(forKey: PrefsManager.KEY_DEVICETYPE)
    }
    func getProductName() -> String{
        let deviceType = UserDefaults.standard.integer(forKey: PrefsManager.KEY_DEVICETYPE)
        if deviceType == Constant.PRODUCT_WALLI{
            return "THE ORIGINAL"
        }
        else if deviceType == Constant.PRODUCT_WALLI_SLIM{
            return "THE SLIM"
        }
        else if deviceType == Constant.PRODUCT_WALLI_TRAVEL{
            return "THE TRAVEL"
        }
        else if deviceType == Constant.PRODUCT_TRACKER{
            return "THE TRAIL"
        }
        else if deviceType == Constant.PRODUCT_KEYFINDER{
            return "KEYFINDER"
        }
        return "THE ORIGINAL"
    }
    func saveUsedWifiList(usedList: [String]){
        UserDefaults.standard.set(usedList, forKey: PrefsManager.KEY_USEDWIFILIST)
    }
    func loadUsedWifiList() -> [String]{
        guard let usedList = UserDefaults.standard.stringArray(forKey: PrefsManager.KEY_USEDWIFILIST)  else { return [String]()}
        return usedList
    }
    func saveHomeWifiSSID(ssid: String){
        UserDefaults.standard.set(ssid, forKey: PrefsManager.KEY_HOMEWIFISSID)
    }
    func getHomeWifiSSID() -> String {
        guard let ssid = UserDefaults.standard.string(forKey: PrefsManager.KEY_HOMEWIFISSID) else { return "" }
        return ssid
    }
    func saveCurrentDeviceName(deviceName: String){
        UserDefaults.standard.set(deviceName, forKey: PrefsManager.KEY_DEVICENAME)
    }
    func getCurrentDeviceName() -> String {
        guard let deviceName = UserDefaults.standard.string(forKey: PrefsManager.KEY_DEVICENAME) else { return "" }
        return deviceName
    }
    func saveWifiSilentZone(mode: Bool){
        UserDefaults.standard.set(mode, forKey: PrefsManager.KEY_WIFISILENTZONEMODE)
    }
    func getWifiSilentZone() ->Bool{
        let wifiSilentZone = UserDefaults.standard.bool(forKey: PrefsManager.KEY_WIFISILENTZONEMODE)
        return wifiSilentZone
    }
    func saveBackPacketMode(mode: Bool){
        UserDefaults.standard.set(mode, forKey: PrefsManager.KEY_BACKPACKETMODE)
    }
    func getBackPacketMode() ->Bool{
        let backPacketMode = UserDefaults.standard.bool(forKey: PrefsManager.KEY_BACKPACKETMODE)
        return backPacketMode
    }
    func saveEmergencyContact(mode: Bool){
        UserDefaults.standard.set(mode, forKey: PrefsManager.KEY_EMERGENCYCONTACT)
    }
    func getEmergencyContactMode() ->Bool{
        let contactMode = UserDefaults.standard.bool(forKey: PrefsManager.KEY_EMERGENCYCONTACT)
        return contactMode
    }
    func saveContactMyName(name: String){
        UserDefaults.standard.set(name, forKey: PrefsManager.KEY_CONTACTMYNAME)
    }
    func getContactMyName() ->String{
        guard let contactName = UserDefaults.standard.string(forKey: PrefsManager.KEY_CONTACTMYNAME) else { return "" }
        return contactName
    }
    func saveContactName1(name: String){
        UserDefaults.standard.set(name, forKey: PrefsManager.KEY_CONTACTNAME1)
    }
    func getContactName1() ->String{
        guard let contactName = UserDefaults.standard.string(forKey: PrefsManager.KEY_CONTACTNAME1) else { return "" }
        return contactName
    }
    func saveContactName2(name: String){
        UserDefaults.standard.set(name, forKey: PrefsManager.KEY_CONTACTNAME2)
    }
    func getContactName2() ->String{
        guard let contactName = UserDefaults.standard.string(forKey: PrefsManager.KEY_CONTACTNAME2) else { return "" }
        return contactName
    }
    func getContactNumber1() ->String{
        guard let contactNumber = UserDefaults.standard.string(forKey: PrefsManager.KEY_CONTACTNUMBER1) else { return "" }
        return contactNumber
    }
    func saveContactNumber1(name: String){
        UserDefaults.standard.set(name, forKey: PrefsManager.KEY_CONTACTNUMBER1)
    }
    func getContactNumber2() ->String{
        guard let contactNumber = UserDefaults.standard.string(forKey: PrefsManager.KEY_CONTACTNUMBER2) else { return "" }
        return contactNumber
    }
    func saveContactNumber2(name: String){
        UserDefaults.standard.set(name, forKey: PrefsManager.KEY_CONTACTNUMBER2)
    }
    func getEmergencySMS() ->Int {
        let counter = UserDefaults.standard.integer(forKey: PrefsManager.KEY_EMERGENCY_SMS)
        return counter
    }
    func setEmergencySMS(value: Int){
        UserDefaults.standard.set(value, forKey: PrefsManager.KEY_EMERGENCY_SMS)
    }
    
    func updateWalli(walliR: WalliR?){
        if(walliR != nil){
            self.mSqliteDB.update(record: walliR!)
        }
    }
    func saveWalli(walliR: WalliR){
        if(self.mDeviceMap == nil){
            return
        }
        let macAddr = walliR.getMacAddress()
        let existWalli = self.mDeviceMap![macAddr]
        // insert new walli
        if(existWalli == nil){
            print("Insert new Walli into List...")
            self.mSqliteDB.insert(record: walliR)
            self.mDeviceMap![macAddr] = walliR
        }
        else{
            print("Walli is already existing...")
        }
    }
    func deleteWalli(walliR: WalliR){
        if(self.mDeviceMap == nil){
            return
        }
        let macAddr = walliR.getMacAddress()
        let existWalli = self.mDeviceMap![macAddr]
        //delete walli
        if(existWalli != nil){
            self.mSqliteDB.delete(MacAddress: macAddr)
            self.mDeviceMap?.removeValue(forKey: macAddr)
        }
    }
    func getDeviceCount() -> Int{
        let devicemap = getDeviceMap()
        return devicemap.count
    }
    
    func getDeviceMap() -> Dictionary<String, WalliR>{
        if(self.mDeviceMap == nil){
            do{
                try self.mDeviceMap = mSqliteDB.readAll()
            }
            catch{
            
            }
        }
        return self.mDeviceMap!
    }
    func containsPeripheral(peripheral: CBPeripheral) -> Bool{
        let deviceMap = Array(PrefsManager.shared.getDeviceMap().values)
        for walliR in deviceMap{
            if walliR.macaddress == peripheral.identifier.uuidString{
                return true
            }
        }
        return false
    }
    func setLastLocation(loc: CLLocation){
        self.mLastLocation = loc
    }
    func getLastLocation() -> CLLocation{
        if self.mLastLocation == nil{
            self.mLastLocation = CLLocation()
        }
        return self.mLastLocation
    }

}
