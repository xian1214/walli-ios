//
//  BaseViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/26/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
 
    @IBAction func onBtnBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}
extension UIViewController {
    var appDelegate: AppDelegate {
    //return UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate
        return UIApplication.shared.delegate as! AppDelegate
    }
}
