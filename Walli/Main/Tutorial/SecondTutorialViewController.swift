//
//  SecondTutorialViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class SecondTutorialViewController: UIViewController {

    @IBOutlet weak var ringYourWallet: UILabel!
    
    @IBOutlet weak var ringDescriptionLabel: UILabel!
    
    @IBOutlet weak var cardSketchImageView: UIImageView!
    
    var deviceType: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.deviceType = PrefsManager.shared.getDeviceType()
        self.ringDescriptionLabel.setLineHeight(lineHeight: 6)
        if(self.deviceType == Constant.PRODUCT_WALLI) {
            self.ringDescriptionLabel.text = "On the main screen of the app, tap the tile \"THE ORIGINAL\" and find the \"Ring\" button. Press this button anytime you need to locate your wallet when it is nearby."
            self.ringYourWallet.text = "Ring Your Wallet(if it is nearby.)"
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch_original")
        }
        else if(self.deviceType == Constant.PRODUCT_WALLI_TRAVEL) {
            self.ringDescriptionLabel.text = "On the main screen of the app, tap the tile \"The TRAVEL\" and find the \"Ring\" button. Press this button anytime you need to locate your wallet when it is nearby."
            self.ringYourWallet.text = "Ring Your Wallet(if it is nearby)"
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch_travel")
        }
        else if(self.deviceType == Constant.PRODUCT_KEYFINDER) {
            self.ringDescriptionLabel.text = "On the main screen of the app, tap the tile \"KEYFINDER\" and look for the \"Ring\" button. Press this button anytime you need to locate your Keyfinder when it is nearby."
            self.ringYourWallet.text = "Ring Your Key Finder(when it is nearby)"
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch")
        }
        else if(self.deviceType == Constant.PRODUCT_TRACKER) {
            self.ringDescriptionLabel.text = "Tap on the tile which says THE TRAIL in the main screen of the app and you should see a Ring button. Press that Ring button anytime you want to locate your wallet."
            self.ringYourWallet.text = "Ring Your Wallet"
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch_pebble")
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
