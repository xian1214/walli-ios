//
//  FourthTutorialViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth


class FourthTutorialViewController: BaseViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cardTopLabel: UILabel!
    
    @IBOutlet weak var cardBottomLabel: UILabel!
    
    @IBOutlet weak var cardImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.pvc = self
        // Do any additional setup after loading the view.
        let deviceType = PrefsManager.shared.getDeviceType()
        self.cardTopLabel.setLineHeight(lineHeight: 6)
        self.cardBottomLabel.setLineHeight(lineHeight: 6)
        if deviceType == Constant.PRODUCT_WALLI {
            titleLabel.text = "Misplaced Card Notification"
            cardTopLabel.text = "Insert your most frequently used card in the topmost slot on the left side of the wallet. Leave in the slot for 5 seconds, then remove. You should see this reflected in the app. You will be alerted if your card is not in the pocket."
            cardBottomLabel.text = "You can configure the amount of time between losing the card and when to be notified from the Settings menu."
            //cardImageView.image = UIImage(named: "ic_sketch_cardnotification")
            updateCardNotification(val: 9)
        }
        else if deviceType == Constant.PRODUCT_WALLI_TRAVEL {
            titleLabel.text = "Lost Passport Notification"
            cardTopLabel.text = "Insert your passport into the passport pocket as shown below. Leave in the slot for 5 seconds, then remove.You should see this reflected in the app. If your passport is not in the pocket, you will receive an alert."
            cardBottomLabel.text = "You can configure the amount of time between losing the card and when you'd like to be notified from the Settings menu."
            //cardImageView.image = UIImage(named: "ic_sketch_lostpassport")
            updateCardNotification(val: 9)
        }
        else if deviceType == Constant.PRODUCT_TRACKER {
            titleLabel.text = "Personal Safety"
            cardTopLabel.text = "On the main screen of the app, tap the tile \"THE TRAIL\" and then click Settings. You can now enable the Personal Safety feature and designate emergency contacts.\n\n Whenever you need help or feel unsafe, double long press the button on the W logo. Your current GPS location will be shared immediately with your emergency contacts."
            cardBottomLabel.isHidden = true
            cardImageView.image = UIImage(named: "ic_card_sketch_pebble")
        }
    }
    
    func updateUIForCharacteristic(peripheral: CBPeripheral, characteristic: CBCharacteristic){
        
        let uuidString = peripheral.identifier.uuidString
        let deviceMap = PrefsManager.shared.getDeviceMap()
        guard let walliR = deviceMap[uuidString] else { return }
        
        guard let data = characteristic.value else { return }
        
        let dataArray = [UInt8](data)
        
        let dataValue = dataArray[0]
        
        print("Received: \(dataValue), DeviceType: \(walliR.devicetype) UUID: \(characteristic.uuid.uuidString)")
        if(characteristic.uuid != CBUUID.CHARACTERISTIC_BATTERY_LEVEL_UUID){
            if walliR.devicetype != Constant.PRODUCT_TRACKER && walliR.devicetype != Constant.PRODUCT_KEYFINDER{
                if dataValue == 7{
                    NotificationUtil.generateNotification(type: .NOTIFICATION_ID_FINDPHONE, walliR: walliR)
                }
                else if dataValue == 8{
                    updateCardNotification(val: 8)
                }
                else if dataValue == 9{
                    updateCardNotification(val: 9)
                }
            }
        }
    }
    func updateCardNotification(val: Int) {
        let deviceType = PrefsManager.shared.getDeviceType()
        if deviceType == Constant.PRODUCT_WALLI {
            if val == 9 {
                let logoGif = UIImage.gif(asset: "ic_walli_in")
                self.cardImageView.animationImages = logoGif?.images
                self.cardImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
                self.cardImageView.animationRepeatCount = 1
                self.cardImageView.startAnimating()
            }
            else if val == 8 {
                let logoGif = UIImage.gif(asset: "ic_walli_out")
                self.cardImageView.animationImages = logoGif?.images
                self.cardImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
                self.cardImageView.animationRepeatCount = 1
                self.cardImageView.startAnimating()

            }
        }
        else if deviceType == Constant.PRODUCT_WALLI_TRAVEL {
            if val == 9 {
                let logoGif = UIImage.gif(asset: "ic_passport_in")
                self.cardImageView.animationImages = logoGif?.images
                self.cardImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
                self.cardImageView.animationRepeatCount = 1
                self.cardImageView.startAnimating()

            }
            else if val == 8 {
                let logoGif = UIImage.gif(asset: "ic_passport_out")
                self.cardImageView.animationImages = logoGif?.images
                self.cardImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
                self.cardImageView.animationRepeatCount = 1
                self.cardImageView.startAnimating()
            }

        }
    }

}
