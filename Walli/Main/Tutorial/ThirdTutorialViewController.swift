//
//  ThirdTutorialViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class ThirdTutorialViewController: UIViewController {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locateLabel: UILabel!
    @IBOutlet weak var cardSketchImageView: UIImageView!
    
    var deviceType: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.deviceType = PrefsManager.shared.getDeviceType()
        self.locateLabel.setLineHeight(lineHeight: 6)
        if(self.deviceType == Constant.PRODUCT_WALLI) {
            self.titleLabel.text = "Locate Your Wallet(when it has been lost)"
            self.locateLabel.text = "On the main screen of the app, tap the tile \"THE ORIGINAL\" and find the \"Map\" button. This will give you the last known GPS location of the wallet.\n\nIf your wallet is nearby and connected to the app, use the \"Ring\" button instead."
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch_original")
        }
        else if(self.deviceType == Constant.PRODUCT_WALLI_TRAVEL) {
            self.titleLabel.text = "Locate Your Wallet(when it has been lost)"
            self.locateLabel.text = "On the main screen of the app, tap the tile \"THE TRAVEL\" and find the \"Map\" button. This will give you the last known GPS location of the wallet.\n\nIf your wallet is nearby and connected to the app, use the \"Ring\" button instead."
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch_travel")
        }
        else if(self.deviceType == Constant.PRODUCT_KEYFINDER) {
            self.titleLabel.text = "Locate Your Keyfinder(when it has been lost)"
            self.locateLabel.text = "On the main screen of the app, tap the tile \"KEYFINDER\" and look for the \"Map\" button in order to find the last known GPS location of the keyfinder.\n\nIf your keys is nearby and connected to the app, use the \"Ring\" button instead."
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch")
        }
        else if(self.deviceType == Constant.PRODUCT_TRACKER) {
            self.titleLabel.text = "Locate Your Wallet"
            self.locateLabel.text = "Tap on the tile which says THE TRAIL and click on the \"Map\" button in order to find the last known GPS location of the wallet.\n\nIf your wallet is nearby and connected to the app, then instead use the Ring button to locate it."
            self.cardSketchImageView.image = UIImage(named: "ic_card_sketch_pebble")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
