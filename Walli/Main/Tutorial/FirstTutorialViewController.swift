//
//  FirstTutorialViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class FirstTutorialViewController: UIViewController {

    @IBOutlet weak var findPhoneLabel: UILabel!
    
    @IBOutlet weak var findPhoneImageView: UIImageView!
    
    @IBOutlet weak var findPhoneTop: NSLayoutConstraint!
    
    @IBOutlet weak var findPhoneHeight: NSLayoutConstraint!
    var deviceType: Int? = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        self.deviceType = PrefsManager.shared.getDeviceType()
        self.findPhoneLabel.setLineHeight(lineHeight: 6)
        if(self.deviceType == Constant.PRODUCT_WALLI) {
            self.findPhoneLabel.text = "To find your phone, flip your wallet to the back and double-tap as shown on the picture below. This will ring your phone even if it is on silent mode.\n\nTIP: You should feel a \"button press\" when you tap.The tap interval should be similar to that of double-clicking a mouse."
            self.findPhoneHeight.constant = 320
            self.findPhoneImageView.image = UIImage(named: "ic_sketch_findphone_walli")
            
        }
        else if(self.deviceType == Constant.PRODUCT_WALLI_TRAVEL) {
            self.findPhoneLabel.text = "To find your phone, double-tap as shown in the picture below. This will ring your phone even if it is on silent mode.\n\nTIP: You should feel a \"button press\" when you tap. The tap interval should be similar to that of double-clicking a mouse."
            self.findPhoneHeight.constant = 220
            self.findPhoneImageView.image = UIImage(named: "ic_sketch_findphone_travel")
        }
        else if(self.deviceType == Constant.PRODUCT_KEYFINDER) {
            self.findPhoneLabel.text = "To find your phone, tap on the button on the keyfinder as shown on the picture below.\n\nThis will ring your phone even on silent mode."
            self.findPhoneTop.constant = 60
            self.findPhoneHeight.constant = 200
            self.findPhoneImageView.image = UIImage(named: "ic_sketch_findphone_keyfinder")
        }
        else if(self.deviceType == Constant.PRODUCT_TRACKER) {
            self.findPhoneLabel.text = "To find your phone, tap once as shown on the picture below. This will ring your phone even if it is on silent mode.\n\nTIP: You should feel a \"button press\" when you tap on the W logo."
            let logoGif = UIImage.gif(asset: "ic_pebble_turnon")
            self.findPhoneImageView.animationImages = logoGif?.images
            self.findPhoneHeight.constant = 250
            self.findPhoneImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
//            self.usageImageView.animationRepeatCount = 1
            self.findPhoneImageView.startAnimating()
        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
