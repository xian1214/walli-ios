//
//  FinalTutorialViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class FinalTutorialViewController: UIViewController {

    @IBOutlet weak var descriptionLable: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.descriptionLable.setLineHeight(lineHeight: 6)
        let deviceType = PrefsManager.shared.getDeviceType()
        if deviceType == Constant.PRODUCT_TRACKER {
            self.descriptionLable.text = "Enjoy being protected with the Trail!"
        }
    }
    

    @IBAction func onPressedNext(_ sender: Any) {
        gotoMainController()
    }
    
    func gotoMainController(){
        BLEManager.getSharedBLEManager().setRunngMode(mode: .checking)
        self.navigationController?.popToRootViewController(animated: true)
    }
}
