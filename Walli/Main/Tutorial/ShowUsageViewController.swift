//
//  ShowUsageViewController.swift
//  Walli
//
//  Created by devstar on 10/7/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class ShowUsageViewController: UIViewController {

    @IBOutlet weak var usageDescriptionLabel: UILabel!
    
    @IBOutlet weak var usageImageView: UIImageView!
    
    @IBOutlet weak var usageImageViewHeight: NSLayoutConstraint!
    var deviceType: Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.deviceType = PrefsManager.shared.getDeviceType()
        self.refreshUsageView()
    }
    
    func refreshUsageView() {
        self.usageDescriptionLabel.setLineHeight(lineHeight: 6)
        if deviceType == Constant.PRODUCT_WALLI{
            self.usageImageView.image = UIImage(named: "ic_usage_walli")
        }
        else if deviceType == Constant.PRODUCT_WALLI_TRAVEL{
            self.usageImageView.image = UIImage(named: "ic_usage_travel")
        }
        else if deviceType == Constant.PRODUCT_KEYFINDER{
            self.usageDescriptionLabel.text = "Press and Hold the button until you hear a beep."
            self.usageImageViewHeight.constant = 200
            self.usageImageView.image = UIImage(named: "ic_usage_keyfinder")
        }
        else if deviceType == Constant.PRODUCT_TRACKER{
            self.usageDescriptionLabel.text = "Press and Hold the W logo until you hear a beep."
            let logoGif = UIImage.gif(asset: "ic_pebble_press_one")
            self.usageImageView.animationImages = logoGif?.images
            self.usageImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
//            self.usageImageView.animationRepeatCount = 1
            self.usageImageView.startAnimating()

        }


    }
    
    func launchWalliKeyCodeScreen(productId: Int){
        let vc =  self.storyboard?.instantiateViewController(identifier: "addWalliViewController") as! AddWalliViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func launchScanDeviceScreen(productId: Int){
        let vc =  self.storyboard?.instantiateViewController(identifier: "scanDeviceViewController") as! ScanDeviceViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onNextPressed(_ sender: Any) {
        if deviceType == Constant.PRODUCT_WALLI{
            //self.launchWalliKeyCodeScreen(productId: Constant.PRODUCT_WALLI)
            self.launchScanDeviceScreen(productId: Constant.PRODUCT_WALLI)
        }
        else if deviceType == Constant.PRODUCT_WALLI_TRAVEL{
            //self.launchWalliKeyCodeScreen(productId: Constant.PRODUCT_WALLI_TRAVEL)
            self.launchScanDeviceScreen(productId: Constant.PRODUCT_WALLI_TRAVEL)
        }
        else if deviceType == Constant.PRODUCT_KEYFINDER{
            self.launchScanDeviceScreen(productId: Constant.PRODUCT_KEYFINDER)
        }
        else if deviceType == Constant.PRODUCT_TRACKER{
            self.launchScanDeviceScreen(productId: Constant.PRODUCT_TRACKER)
        }
    }
    
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
