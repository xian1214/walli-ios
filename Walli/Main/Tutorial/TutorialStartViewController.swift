//
//  TutorialStartViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class TutorialStartViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onPressedStart(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "tutorialViewController") as! TutorialViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
