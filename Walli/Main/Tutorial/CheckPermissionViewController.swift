//
//  CheckPermissionViewController.swift
//  Walli
//
//  Created by devstar on 10/7/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreLocation

class CheckPermissionViewController: BaseViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var bluetoothView: ExtensionView!
    @IBOutlet weak var notificationView: ExtensionView!
    @IBOutlet weak var locationView: ExtensionView!
    
    @IBOutlet weak var bluetoothImageView: UIImageView!
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var locationImageView: UIImageView!
    
    @IBOutlet weak var bluetoothButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    
    var notificationPermission = false
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.cpvc = self
        NotificationCenter.default.addObserver(self, selector: #selector(refreshStatus), name: UIApplication.willEnterForegroundNotification, object: nil)
        // Do any additional setup after loading the view.
        self.descriptionLabel.setLineHeight(lineHeight: 6)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.refreshStatus()
    }
    @objc func refreshStatus() {
        if(checkBluetoothPermission() == 1){
            bluetoothView.backgroundColor = UIColor(hexString: "#35ADB0")
            bluetoothButton.setTitleColor(UIColor.white, for: .normal)
            bluetoothImageView.image = UIImage(named: "ic_permission_checkmark")
        }
        else{
            bluetoothView.backgroundColor = UIColor.clear
            bluetoothButton.setTitleColor(UIColor(hexString: "#35ADB0"), for: .normal)
            bluetoothImageView.image = UIImage(named: "ic_permission_bluetooth")
        }
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                    self.notificationView.backgroundColor = UIColor(hexString: "#35ADB0")
                    self.notificationButton.setTitleColor(UIColor.white, for: .normal)
                    self.notificationImageView.image = UIImage(named: "ic_permission_checkmark")
                    self.notificationPermission = true
                }
            }
            else{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                    self.notificationView.backgroundColor = UIColor.clear
                    self.notificationButton.setTitleColor(UIColor(hexString: "#35ADB0"), for: .normal)
                    self.notificationImageView.image = UIImage(named: "ic_permission_notification")
                    self.notificationPermission = false
                }
            }
        }

        if(checkLocationPermission() == 1){
            locationView.backgroundColor = UIColor(hexString: "#35ADB0")
            locationButton.setTitleColor(UIColor.white, for: .normal)
            locationImageView.image = UIImage(named: "ic_permission_checkmark")
        }
        else{
            locationView.backgroundColor = UIColor.clear
            locationButton.setTitleColor(UIColor(hexString: "#35ADB0"), for: .normal)
            locationImageView.image = UIImage(named: "ic_permission_location")
        }

    }
    func openAppOrSystemSettingsAlert(title: String, message: String) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    func openAppsLocationServiceAlert(title: String, message: String) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)

    }
    func checkBluetoothPermission() -> Int {
        if #available(iOS 13.1, *) {
            if CBCentralManager.authorization == .notDetermined {
                return 0
            }
            else if CBCentralManager.authorization == .denied {
                return 2
            }
            else if CBCentralManager.authorization == .allowedAlways {
                return 1
            }
            return -1
        }
        else if #available(iOS 13.0, *) {
            if CBCentralManager().authorization == .notDetermined {
                return 0
            }
            else if CBCentralManager().authorization == .denied {
                return 2
            }
            else if CBCentralManager().authorization == .allowedAlways {
                return 1
            }
            return -1
            
        }
        return -1
    }

    func checkLocationPermission() -> Int {
        if CLLocationManager.locationServicesEnabled() {
            print(CLLocationManager.authorizationStatus().rawValue)
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined:
                    print("No access")
                    return 0
                case .restricted, .denied:
                    print("Access denied")
                    return 2
                case .authorizedAlways:
                    print("Access")
                    return 1
                case .authorizedWhenInUse:
                    openAppOrSystemSettingsAlert(title: "Please select \"Always\" in the Notification Settings in order for Walli App to work properly. Your location information is always private and secure.", message: "")
                    return 3
                @unknown default:
                break
            }
        } else {
            return -1
        }
        return 1
    }
    
    @IBAction func onBluetoothPressed(_ sender: Any) {
        let permission = checkBluetoothPermission()
        if(permission == 0) {
            BLEManager.getSharedBLEManager().initCentralManager()
        }
        else if(permission == 2) {
            print("Bluetooth permission denied")
            openAppOrSystemSettingsAlert(title: "Bluetooth permission is currently disabled for the application. Enable Bluetooth from the application settings", message: "")
        }
    }
    
    @IBAction func onNotificationPressed(_ sender: Any) {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print(settings.authorizationStatus.rawValue)
            
            if settings.authorizationStatus == .notDetermined {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                    if granted{
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                            self.notificationView.backgroundColor = UIColor(hexString: "#35ADB0")
                            self.notificationButton.setTitleColor(UIColor.white, for: .normal)
                            self.notificationImageView.image = UIImage(named: "ic_permission_checkmark")
                            self.notificationPermission = true
                        }
                    }
                    else{
                        print("Permission denined for local notifications.")
                    }
                }
            }
            else if settings.authorizationStatus != .authorized {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
                    self.openAppOrSystemSettingsAlert(title: "Notification permission is currently disabled for the application. Enable notification from the application settings", message: "")
                }
            }
        }
    }
    
    @IBAction func onLocationPressed(_ sender: Any) {
        let permission = checkLocationPermission()
        if( permission == 0) {
            LocationManager.getSharedManager().initLocationManager()
        }
        else if( permission == -1) {
            openAppsLocationServiceAlert(title: "Location Service is currently disabled for the application. Enable Location from the privacy settings", message: "")
        }
        else if( permission == 2 || permission == 3) {
            openAppOrSystemSettingsAlert(title: "Please select \"Always\" in the Notification Settings in order for Walli App to work properly. Your location information is always private and secure.", message: "")
        }
    }
    
    
    @IBAction func onPressedNext(_ sender: Any) {
        if(checkBluetoothPermission() != 1){
            Util.showAlert(vc: self, "Please check the app permissions and try again" , "")
            return
        }
        if(checkLocationPermission() != 1){
            Util.showAlert(vc: self, "Please check the app permissions and try again" , "")
            return
        }
        if(self.notificationPermission == false) {
            Util.showAlert(vc: self, "Please check the app permissions and try again" , "")
            return
        }
        if BLEManager.getSharedBLEManager().centralManager == nil {
            BLEManager.getSharedBLEManager().initCentralManager()
        }
        LocationManager.getSharedManager().initLocationManager()
        let vc =  self.storyboard?.instantiateViewController(identifier: "showUsageViewController") as! ShowUsageViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
