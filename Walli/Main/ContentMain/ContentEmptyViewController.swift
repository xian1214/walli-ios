//
//  ContentEmptyViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/26/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class ContentEmptyViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickAddDevice(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "selectProductsViewController") as! SelectProductsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
