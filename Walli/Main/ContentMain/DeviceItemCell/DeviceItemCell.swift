//
//  DeviceItemCell.swift
//  Walli
//
//  Created by Xian Huang on 1/30/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class DeviceItemCell: UITableViewCell {

    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var deviceState: UILabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var frontView: UIView!

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewRing: UIView!
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var viewSetting: UIView!
    @IBOutlet weak var imgRing: UIImageView!
    
    
    @IBOutlet weak var deviceWidth: NSLayoutConstraint!
    @IBOutlet weak var deviceHeight: NSLayoutConstraint!
    var index: Int!
    var callbackRing: ((_ index: Int) -> ())?
    var callbackMap: ((_ index: Int) -> ())?
    var callbackSetting: ((_ index: Int) -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onPressFrontView(_ sender: Any) {
        UIView.transition(with: self.contentView, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromRight, animations: {
//            self.contentView.insertSubview(self.backView, aboveSubview: self.frontView)
            self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0)
            self.frontView.isHidden = true
        }) { (finished) in
            
        }
    }
    
    @IBAction func onPressBackView(_ sender: Any) {
       UIView.transition(with: self.contentView, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromRight, animations: {
        self.frontView.isHidden = false
       }) { (finished) in
           
       }
    }
    @IBAction func onPressRing(_ sender: Any) {
         callbackRing?(index)
    }
    
    @IBAction func onPressMap(_ sender: Any) {
        callbackMap?(index)
    }
    
    @IBAction func onPressSetting(_ sender: Any) {
        callbackSetting?(index)
    }
    
}
