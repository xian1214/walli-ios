//
//  ContentMainViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/26/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth

class ContentMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableDevice: UITableView!
    var deviceMaps: [WalliR]? = nil
    var disconnectItem: DispatchWorkItem?
    var cardoutItem: DispatchWorkItem?
    
    var buttonLongPressed: Int = 0
    var buttonLongItem: DispatchWorkItem?

    var buttonTwicePressed: Int = 0
    var buttonTwiceItem: DispatchWorkItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        print("[Content Main] viewDidLoad")
        appDelegate.cvc = self
        // Do any additional setup after loading the view.
        tableDevice.delegate = self
        tableDevice.dataSource = self
        tableDevice.register(UINib(nibName: "DeviceItemCell", bundle: nil), forCellReuseIdentifier: "_DeviceItemCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        self.deviceMaps = Array(PrefsManager.shared.getDeviceMap().values)        
        print("[Content Main] viewWillAppear")
        tableDevice.reloadData()
    }
    
    @IBAction func onClickAddDevice(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "selectProductsViewController") as! SelectProductsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.deviceMaps!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "_DeviceItemCell", for: indexPath) as! DeviceItemCell
        
        let walliR = self.deviceMaps?[indexPath.row]
        cell.selectionStyle = .none
        cell.index = indexPath.row
        
        switch walliR?.devicetype{
            case Constant.PRODUCT_WALLI:
                cell.backImage.image = UIImage(named: "ic_product_wallet_back_wide")
                if walliR?.cardstatus == WalliR.CardStatus.CARD_STATUS_NORMAL{
                    cell.deviceImage.image = UIImage(named: "ic_product_wallet")
                }
                else{
                    cell.deviceImage.image = UIImage(named: "ic_product_wallet_card")
                }
                cell.deviceWidth.constant = 50
                cell.deviceHeight.constant = 44
                cell.viewBack.backgroundColor = UIColor(hexString: "#4FACAD")
                cell.viewRing.backgroundColor = UIColor(hexString: "#40999A")
                cell.viewMap.backgroundColor = UIColor(hexString: "#4FACAD")
                cell.viewSetting.backgroundColor = UIColor(hexString: "#40999A")
                cell.deviceName.text = "THE ORIGINAL"
                break
            case Constant.PRODUCT_WALLI_SLIM:
                cell.backImage.image = UIImage(named: "ic_product_slim_back_wide")
                if walliR?.cardstatus == WalliR.CardStatus.CARD_STATUS_NORMAL{
                    cell.deviceImage.image = UIImage(named: "ic_product_slim")
                }
                else{
                    cell.deviceImage.image = UIImage(named: "ic_product_slim_card")
                }
                cell.deviceWidth.constant = 50
                cell.deviceHeight.constant = 44
                cell.viewBack.backgroundColor = UIColor(hexString: "#F8C06B")
                cell.viewRing.backgroundColor = UIColor(hexString: "#E7B05C")
                cell.viewMap.backgroundColor = UIColor(hexString: "#F8C06B")
                cell.viewSetting.backgroundColor = UIColor(hexString: "#E7B05C")
                cell.deviceName.text = "THE SLIM"
                break
            case Constant.PRODUCT_WALLI_TRAVEL:
                cell.backImage.image = UIImage(named: "ic_product_travel_back_wide")
                if walliR?.cardstatus == WalliR.CardStatus.CARD_STATUS_NORMAL{
                    cell.deviceImage.image = UIImage(named: "ic_product_travel")
                }
                else{
                    cell.deviceImage.image = UIImage(named: "ic_product_travel_passport")
                }
                cell.deviceWidth.constant = 50
                cell.deviceHeight.constant = 44
                cell.viewBack.backgroundColor = UIColor(hexString: "#2F7CB0")
                cell.viewRing.backgroundColor = UIColor(hexString: "#366E94")
                cell.viewMap.backgroundColor = UIColor(hexString: "#2F7CB0")
                cell.viewSetting.backgroundColor = UIColor(hexString: "#366E94")
                cell.deviceName.text = "THE TRAVEL"
                break
            case Constant.PRODUCT_TRACKER:
                cell.backImage.image = UIImage(named: "ic_product_pebble_back_wide")
                cell.deviceImage.image = UIImage(named: "ic_product_pebble")
                cell.deviceWidth.constant = 55
                cell.deviceHeight.constant = 55
                cell.viewBack.backgroundColor = UIColor(hexString: "#A32159")
                cell.viewRing.backgroundColor = UIColor(hexString: "#762146")
                cell.viewMap.backgroundColor = UIColor(hexString: "#A32159")
                cell.viewSetting.backgroundColor = UIColor(hexString: "#762146")
                cell.deviceName.text = "THE TRAIL"
                break
            case Constant.PRODUCT_KEYFINDER:
                cell.backImage.image = UIImage(named: "ic_product_keyfinder_back_wide")
                cell.deviceWidth.constant = 50
                cell.deviceHeight.constant = 44
                cell.deviceImage.image = UIImage(named: (walliR?.getTrackImage())!)
                cell.viewBack.backgroundColor = UIColor(hexString: "#28CACB")
                cell.viewRing.backgroundColor = UIColor(hexString: "#19AEAF")
                cell.viewMap.backgroundColor = UIColor(hexString: "#28CACB")
                cell.viewSetting.backgroundColor = UIColor(hexString: "#19AEAF")
                cell.deviceName.text = walliR?.devicename
                break
            default:
                break
            
        }
        if(walliR?.peripheral != nil){
            if(walliR?.peripheral?.state == CBPeripheralState.connected){
                cell.deviceState.text = "Connected"
                // Request to read battery level
                BLEManager.getSharedBLEManager().readBatteryStatus(peripheral: (walliR?.peripheral)!)
            }
            else if(walliR?.peripheral?.state == CBPeripheralState.connecting){
                cell.deviceState.text = "Connecting"
            }
            else{
                cell.deviceState.text = "Disconnected"
            }
            
        }
        else{
            cell.deviceState.text = "Disconnected"
        }
        if(walliR?.isRinging == false){
            cell.imgRing.stopAnimating()
            cell.imgRing.image = UIImage(named: "ic_device_ring")
        }
        else{
            let logoGif = UIImage.gif(asset: "ic_device_ring_animation")
            cell.imgRing.animationImages = logoGif?.images
            cell.imgRing.animationRepeatCount = 0
            cell.imgRing.startAnimating()
        }
        cell.callbackRing = { (index) in
            if(walliR?.peripheral != nil){
                if(walliR!.isRinging == false){
                    BLEManager.getSharedBLEManager().sendRingStart(peripheral: walliR!.peripheral!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        BLEManager.getSharedBLEManager().sendRingStop(peripheral: walliR!.peripheral!)
                    }
                }
                else{
                    BLEManager.getSharedBLEManager().sendRingStop(peripheral: walliR!.peripheral!)
                }
            }
        }
        cell.callbackMap = { (index) in
            if walliR!.isWalliConnected(){
                Util.showAlert(vc: self, "Tip", "Your smart device is already connected to your phone. Please use the \"Ring\" feature to locate it.\n\nA map with the Last Known Location of your smart device will only be displayed when it is out of range and no longer connected to your phone.")
            }
            else{
                let vc =  self.storyboard?.instantiateViewController(identifier: "mapViewController") as! MapViewController
                vc.setWalli(walliR: walliR!)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        cell.callbackSetting = { (index) in
            let vc =  self.storyboard?.instantiateViewController(identifier: "settingsViewController") as! SettingsViewController
            vc.setWalli(walliR: walliR!)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func updateUIForReload(){
        tableDevice.reloadData()
    }
    func updateUIForConnected(peripheral: CBPeripheral, state: Bool){
        tableDevice.reloadData()
        let uuidString = peripheral.identifier.uuidString
        let deviceMap = PrefsManager.shared.getDeviceMap()
        guard let walliR = deviceMap[uuidString] else { return }
//        print("Device Connection State Changed \(peripheral.state.rawValue), \(walliR.peripheral?.state.rawValue)")

        if(state == false){
            // in case disconnected
            LocationManager.getSharedManager().startUpdatingLocation()
            disconnectItem = DispatchWorkItem(block: {
                print("Disconnected Notification Started.")
                if self.checkWifiSilentMode() == false{
                    NotificationUtil.generateNotification(type: .NOTIFICATION_ID_DISCONNECTED, walliR: walliR)
                }
            })
            var disconnectDelay = 0
            if walliR.rangemode == WalliR.RANGE_MODERATE{
                disconnectDelay = 30000
            }
            else if walliR.rangemode == WalliR.RANGE_FAR{
                disconnectDelay = 600000
            }
            print("Disconnected Notification Requested: \(disconnectDelay)")
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(disconnectDelay), execute: disconnectItem!)
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                let lastLoc = PrefsManager.shared.getLastLocation()
//                print("Saving Last Location \(lastLoc.coordinate.latitude), \(lastLoc.coordinate.longitude)")
                walliR.setLostLatitude(latitude: lastLoc.coordinate.latitude)
                walliR.setLostLongitude(longitude: lastLoc.coordinate.longitude)
                let militime = Int(Date().timeIntervalSince1970)
                walliR.setLostTime(losttime: militime)
                PrefsManager.shared.updateWalli(walliR: walliR)
//                let timeInterval = Double(walliR.losttime)
//                let myNDate = Date(timeIntervalSince1970: timeInterval)
//                print("Last Time: \(militime),  \(Util.convertTimeStampToLiteral(date: myNDate))")
            }

        }
        else{
            //connected
            disconnectItem?.cancel()
            if self.checkWifiSilentMode() == false{
                NotificationUtil.generateNotification(type: .NOTIFICATION_ID_CONNECTED, walliR: walliR)
            }
        }

    }
    func checkWifiSilentMode() -> Bool{
        let wifiSilentZone = PrefsManager.shared.getWifiSilentZone()
        guard let wifiSSID = WifiUtil.fetchCurrentSSIDInfo() else { return false}
        if wifiSilentZone == true && wifiSSID == PrefsManager.shared.getHomeWifiSSID(){
            return true
        }
        return false
    }
    func updateUIForRing(peripheral: CBPeripheral, bRingStart: Bool){
        let uuidString = peripheral.identifier.uuidString
        let deviceMap = PrefsManager.shared.getDeviceMap()
        guard let walliR = deviceMap[uuidString] else { return }

        walliR.isRinging = bRingStart
        tableDevice.reloadData()

    }
    func startEmergencySMS(){
        let emergencyMode = PrefsManager.shared.getEmergencyContactMode()
        if emergencyMode == false {
            return
        }
        PrefsManager.shared.setEmergencySMS(value: 1)
        LocationManager.getSharedManager().startUpdatingLocation()
    }
    func updateUIForCharacteristic(peripheral: CBPeripheral, characteristic: CBCharacteristic){
        
        let uuidString = peripheral.identifier.uuidString
        let deviceMap = PrefsManager.shared.getDeviceMap()
        guard let walliR = deviceMap[uuidString] else { return }
        
        guard let data = characteristic.value else { return }
        
        let dataArray = [UInt8](data)
        
        let dataValue = dataArray[0]
        
        print("Received: \(dataValue), DeviceType: \(walliR.devicetype) UUID: \(characteristic.uuid.uuidString)")
        if(characteristic.uuid == CBUUID.CHARACTERISTIC_BATTERY_LEVEL_UUID){
            walliR.battery = Int(dataValue)
        }
        else{
            if walliR.devicetype == Constant.PRODUCT_TRACKER || walliR.devicetype == Constant.PRODUCT_KEYFINDER{
                if dataValue == 8{
                    NotificationUtil.generateNotification(type: .NOTIFICATION_ID_FINDPHONE, walliR: walliR)
                }
                else if dataValue == 2{
                    buttonLongItem = DispatchWorkItem(block: {
                        print("Ring Phone State released.")
                        self.buttonLongPressed = 0
                    })
                    if buttonLongPressed == 0 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: buttonLongItem!)
                    }
                    self.buttonLongPressed += 1
                    if(self.buttonLongPressed >= 2){
                        print("Long button pressed.")
                        startEmergencySMS()
                        buttonLongPressed = 0
                    }
                }
            }
            else{
                if dataValue == 7{
                    if(PrefsManager.shared.getBackPacketMode() == false){
                        NotificationUtil.generateNotification(type: .NOTIFICATION_ID_FINDPHONE, walliR: walliR)
                    }
                    else {
                        print("Ring phone button \(self.buttonTwicePressed)")
                        buttonTwiceItem = DispatchWorkItem(block: {
                            print("Ring Phone State released.")
                            self.buttonTwicePressed = 0
                        })
                        if self.buttonTwicePressed == 0 {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: buttonTwiceItem!)
                        }
                        self.buttonTwicePressed += 1
                        if self.buttonTwicePressed >= 2 {
                            NotificationUtil.generateNotification(type: .NOTIFICATION_ID_FINDPHONE, walliR: walliR)
                            self.buttonTwicePressed = 0
                        }
                    }
                }
                else if dataValue == 8{
                    walliR.cardstatus = .CARD_STATUS_OUT
                    tableDevice.reloadData()
                    cardoutItem = DispatchWorkItem(block: {
                        print("Card Out Notfication Started")
                        NotificationUtil.generateNotification(type: .NOTIFICATION_ID_CARDOUT, walliR: walliR)
                    })
                    print("Card Out Notfication Requested: \(walliR.cardnotificationdelay)")
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000 * walliR.cardnotificationdelay), execute: cardoutItem!)
                }
                else if dataValue == 9{
                    walliR.cardstatus = .CARD_STATUS_NORMAL
                    tableDevice.reloadData()
                    NotificationUtil.removePendingNotification(identifier: "notifier_cardout")
                    cardoutItem?.cancel()
                }
            }
        }
    }
}
