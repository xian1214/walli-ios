//
//  MapViewController.swift
//  Walli
//
//  Created by Xian Huang on 2/3/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: BaseViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    var walliR: WalliR? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.showsUserLocation = true
        mapView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var span = MKCoordinateSpan()
        span.latitudeDelta = 0.005
        span.longitudeDelta = 0.005
        var location = CLLocationCoordinate2D()
        location.latitude = walliR!.latitude
        location.longitude = walliR!.longitude
        
        let timeInterval = Double(walliR!.losttime)
        let myNDate = Date(timeIntervalSince1970: timeInterval)
        
        let region = MKCoordinateRegion(center: location, span: span)
        self.mapView.setRegion(region, animated: true)

        let annotationTitle = walliR!.devicename + "-" + Util.convertTimeStampToLiteral(date: myNDate)
        var annotationMark = MKPointAnnotation()
        annotationMark.subtitle = annotationTitle
        annotationMark.coordinate = location
        self.mapView.addAnnotation(annotationMark)

        print("Mark: \(annotationMark) Location: \(location.latitude), \(location.longitude)")

    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is MKPointAnnotation){
            return nil
        }
        
        let reuseId = "Walli"
        let anView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
        
        switch walliR?.devicetype {
        case Constant.PRODUCT_WALLI:
            anView.glyphImage = UIImage(named:"ic_product_wallet")
        case Constant.PRODUCT_WALLI_SLIM:
            anView.glyphImage = UIImage(named:"ic_product_slim")
        case Constant.PRODUCT_WALLI_TRAVEL:
            anView.glyphImage = UIImage(named:"ic_product_travel")
        case Constant.PRODUCT_TRACKER:
            anView.glyphImage = UIImage(named:"ic_product_pebble")
        case Constant.PRODUCT_KEYFINDER:
            anView.glyphImage = UIImage(named:"ic_product_keyfinder")
        default:
            break
        }
        anView.canShowCallout = true

        return anView
    }
    func setWalli(walliR: WalliR){
        print("[Settings] Walli Information Setted")
        self.walliR = walliR
    }

}
