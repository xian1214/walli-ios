//
//  AddWalliViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/27/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class AddWalliViewController: BaseViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var labelWalliTitle: UILabel!
    @IBOutlet weak var textWalliCode: UITextField!
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var butInform: UIButton!
    var deviceType: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.textWalliCode.delegate = self
        
        deviceType = PrefsManager.shared.getDeviceType()
        let productName = PrefsManager.shared.getProductName()
        self.labelWalliTitle.text = "Add \(productName)"

/*
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.setBackgroundImage(UIImage(), forToolbarPosition: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        toolbar.setShadowImage(UIImage(),
        forToolbarPosition: UIBarPosition.any)
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(doneClicked))
        doneButton.tintColor = UIColor.black
        toolbar.setItems([flexibleSpace, doneButton], animated: false)
        self.textWalliCode.inputAccessoryView = toolbar
 */
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    @IBAction func onTouchInform(_ sender: UIButton) {
        showToolTip(tip: Constant.TIP_WALLICODE, view: sender)
    }
    
    func showToolTip(tip: String, view: UIView){
        let vc =  self.storyboard?.instantiateViewController(identifier: "popupViewController") as! PopupViewController
        vc.modalPresentationStyle = .popover
        
        if let popover = vc.popoverPresentationController {

           let viewForSource = view
           popover.sourceView = viewForSource

           // the position of the popover where it's showed
           popover.sourceRect = viewForSource.bounds
            
            let labelSize = tip.size(withAttributes: [.font: UIFont.systemFont(ofSize: 14)])
            
            vc.preferredContentSize = CGSize(width: 200, height: labelSize.height + 20)
           // the size you want to display
            vc.setTipText(tip: tip)
           popover.delegate = self
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    @IBAction func onTouchSubmit(_ sender: Any) {
        let walliCode = self.textWalliCode.text
        if walliCode!.count < 6{
            Util.showAlert(vc: self, "Walli", "Please check the walli code.")
            return
        }
        
        var deviceName: String = Constant.PREFIX_WALLI + walliCode!
        if deviceType == Constant.PRODUCT_WALLI{
            deviceName = Constant.PREFIX_WALLI + walliCode!
        }
        else if deviceType == Constant.PRODUCT_WALLI_SLIM{
            deviceName = Constant.PREFIX_SLIM + walliCode!
        }
        else if deviceType == Constant.PRODUCT_WALLI_TRAVEL{
            deviceName = Constant.PREFIX_TRAVEL + walliCode!;
        }
        PrefsManager.shared.saveCurrentDeviceName(deviceName: deviceName)
        let vc =  self.storyboard?.instantiateViewController(identifier: "scanDeviceViewController") as! ScanDeviceViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onTouchLostCode(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "lostCodeViewController") as! LostCodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    @objc func doneClicked(){
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
