//
//  LostCodeViewController.swift
//  Walli
//
//  Created by Xian Huang on 2/7/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth

class LostCodeViewController: UIViewController {
    
    @IBOutlet weak var imgSync: UIImageView!
    var scanItem: DispatchWorkItem?
    var deviceType: Int? = 0
    var failedCount: Int? = 0
    var prefixKey: String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        let sonargif = UIImage.gif(asset: "sonar")
        self.imgSync.animationImages = sonargif?.images
        self.imgSync.animationRepeatCount = 0
        appDelegate.lvc = self

    }
    override func viewWillAppear(_ animated: Bool) {
        self.deviceType = PrefsManager.shared.getDeviceType()
        switch self.deviceType {
        case Constant.PRODUCT_WALLI:
            self.prefixKey = Constant.PREFIX_WALLI
        case Constant.PRODUCT_WALLI_SLIM:
            self.prefixKey = Constant.PREFIX_SLIM
        case Constant.PRODUCT_WALLI_TRAVEL:
            self.prefixKey = Constant.PREFIX_WALLI
        default:
            self.prefixKey = Constant.PREFIX_WALLI
            break
        }
        BLEManager.getSharedBLEManager().setWalliPrefix(prefix: self.prefixKey!)
        
    }

    func updateWalliCode(walliCode: String){
        self.imgSync.stopAnimating()
        scanItem?.cancel()
        Util.showAlert(vc: self, "Walli", "Your code is " + walliCode)
    }
    @IBAction func onClickedSync(_ sender: Any) {
         let centralManager = BLEManager.getSharedBLEManager().centralManager
         if centralManager?.state != .poweredOn{
             Util.showAlert(vc: self, "Walli", "Please make sure that Bluetooth is turned on.")
         }
         else{
             self.imgSync.startAnimating()
            BLEManager.getSharedBLEManager().setRunngMode(mode: .lostcode)
            scanItem = DispatchWorkItem(block: {
                BLEManager.getSharedBLEManager().stopScan()
                self.imgSync.stopAnimating()
                self.failedCount = self.failedCount! + 1;
                if self.failedCount! > 2{
                    Util.showAlert(vc: self, "Walli", "Please make sure you have plugged in the battery on " + self.prefixKey!)
                    self.failedCount = 0
                }
                print("Scanning failed.")

            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 7.0, execute: scanItem!)
         }
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        BLEManager.getSharedBLEManager().setRunngMode(mode: .checking)
        self.navigationController?.popViewController(animated: true)
    }
    

}
