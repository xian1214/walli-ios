//
//  PopupViewController.swift
//  Walli
//
//  Created by Xian Huang on 2/3/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class PopupViewController: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    var textTip: String? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.labelText.text = self.textTip!
        // Do any additional setup after loading the view.
    }
    
    func setTipText(tip: String){
        self.textTip = tip
    }
}
