//
//  SelectProductsViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/26/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class SelectProductsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onTouchOriginal(_ sender: Any) {
        //launchWalliKeyCodeScreen(productId: Constant.PRODUCT_WALLI)
        PrefsManager.shared.saveCurrentDeviceName(deviceName: Constant.PREFIX_WALLI)
        launchScanDeviceScreen(productId: Constant.PRODUCT_WALLI)
    }

    func gotoTutorialScreen() {
        let vc =  self.storyboard?.instantiateViewController(identifier: "tutorialStartViewController") as! TutorialStartViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func onTouchSlim(_ sender: Any) {
        //launchWalliKeyCodeScreen(productId: Constant.PRODUCT_WALLI_SLIM)
        PrefsManager.shared.saveCurrentDeviceName(deviceName: Constant.PREFIX_SLIM)
        launchScanDeviceScreen(productId: Constant.PRODUCT_WALLI_SLIM)
    }
    @IBAction func onTouchTravel(_ sender: Any) {
        //launchWalliKeyCodeScreen(productId: Constant.PRODUCT_WALLI_TRAVEL)
        PrefsManager.shared.saveCurrentDeviceName(deviceName: Constant.PREFIX_TRAVEL)
        launchScanDeviceScreen(productId: Constant.PRODUCT_WALLI_TRAVEL)
    }
    
    @IBAction func onTouchPebble(_ sender: Any) {
        PrefsManager.shared.saveCurrentDeviceName(deviceName: Constant.PREFIX_TRACKER)
        launchScanDeviceScreen(productId: Constant.PRODUCT_TRACKER)
    }
    
    @IBAction func onTouchKeyFinder(_ sender: Any) {
        PrefsManager.shared.saveCurrentDeviceName(deviceName: Constant.PREFIX_TRACKER)
        launchScanDeviceScreen(productId: Constant.PRODUCT_KEYFINDER)
//        PrefsManager.shared.setDeviceType(deviceType: Constant.PRODUCT_KEYFINDER)
//        PrefsManager.shared.saveCurrentDeviceName(deviceName: Constant.PREFIX_KEYFINDER)
//        let vc =  self.storyboard?.instantiateViewController(identifier: "checkPermissionViewController") as! CheckPermissionViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
//        let vc =  self.storyboard?.instantiateViewController(identifier: "scanKeyFinderViewController") as! ScanKeyFinderViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func launchWalliKeyCodeScreen(productId: Int){
        PrefsManager.shared.setDeviceType(deviceType: productId)

 //       gotoTutorialScreen()
        
        let vc =  self.storyboard?.instantiateViewController(identifier: "checkPermissionViewController") as! CheckPermissionViewController
        self.navigationController?.pushViewController(vc, animated: true)
 

//        let vc =  self.storyboard?.instantiateViewController(identifier: "addWalliViewController") as! AddWalliViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }

    func launchScanDeviceScreen(productId: Int){
        PrefsManager.shared.setDeviceType(deviceType: productId)
        //gotoTutorialScreen()
        
        let vc =  self.storyboard?.instantiateViewController(identifier: "checkPermissionViewController") as! CheckPermissionViewController
        self.navigationController?.pushViewController(vc, animated: true)
 

//        let vc =  self.storyboard?.instantiateViewController(identifier: "scanDeviceViewController") as! ScanDeviceViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}
