//
//  ScanKeyFinderViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/28/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth

class ScanKeyFinderViewController: BaseViewController {

    @IBOutlet weak var imgSync: UIImageView!

    var deviceArray: NSArray?
        
    var deviceType: Int? = 0
    var deviceName: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // sonar view
        let sonargif = UIImage.gif(asset: "sonar")
        self.imgSync.animationImages = sonargif?.images
        self.imgSync.animationRepeatCount = 0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.deviceType = PrefsManager.shared.getDeviceType()
        self.deviceName = PrefsManager.shared.getCurrentDeviceName()
    }
    @IBAction func onClickedScan(_ sender: Any) {
        let centralManager = BLEManager.getSharedBLEManager().centralManager
        if centralManager?.state != .poweredOn{
            Util.showAlert(vc: self, "Walli", "Please make sure that Bluetooth is turned on.")
        }
        else{
            self.imgSync.startAnimating()
            BLEManager.getSharedBLEManager().setRunngMode(mode: .scanning)
            DispatchQueue.main.asyncAfter(deadline: .now() + 7.0) {
                BLEManager.getSharedBLEManager().stopScan()
                self.imgSync.stopAnimating()
                if(BLEManager.getSharedBLEManager().discoveredBLE!){
                    print("Scanning success.")
                    let macAddr = BLEManager.getSharedBLEManager().currentPeripheral?.identifier.uuidString
                    let walliR = WalliR(devicetype: self.deviceType!)
                    walliR.setMacAddress(macAddr: macAddr!)
                    walliR.peripheral = BLEManager.getSharedBLEManager().currentPeripheral
                    walliR.setDeviceName(devicename: PrefsManager.shared.getProductName())
                    PrefsManager.shared.saveWalli(walliR: walliR)
                    PrefsManager.shared.mDeviceMap?[macAddr!] = walliR
                    BLEManager.getSharedBLEManager().setRunngMode(mode: .checking)
                    self.onBackPressed()

                }
                else{
                    print("Scanning failed.")
                }
            }
        }
    }
    func onBackPressed(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    

}
