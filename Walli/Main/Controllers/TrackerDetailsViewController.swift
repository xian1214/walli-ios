//
//  TrackerDetailsViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/28/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth

class TrackerDetailsViewController: UIViewController {

    @IBOutlet weak var img_tracker_main: UIImageView!
    @IBOutlet weak var edit_devicename: UITextField!
   
    @IBOutlet weak var ic_checkmark_key: UIImageView!
    @IBOutlet weak var ic_checkmark_remote: UIImageView!
    @IBOutlet weak var ic_checkmark_luggage: UIImageView!
    @IBOutlet weak var ic_checkmark_pet: UIImageView!
    @IBOutlet weak var ic_checkmark_bagpack: UIImageView!
    @IBOutlet weak var ic_checkmark_camera: UIImageView!
    @IBOutlet weak var ic_checkmark_car: UIImageView!
    @IBOutlet weak var ic_checkmark_other: UIImageView!
    
    var trackerType: Int? = 0
    var deviceType: Int? = 0
    var deviceName: String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.deviceType = PrefsManager.shared.getDeviceType()
        self.deviceName = PrefsManager.shared.getCurrentDeviceName()
        onClickTrackKey()
    }

    @IBAction func onClickSave(_ sender: Any) {
        self.deviceName = edit_devicename.text
        if(self.deviceName?.isEmpty == false){
            let macAddr = BLEManager.getSharedBLEManager().currentPeripheral?.identifier.uuidString
            let walliR = WalliR(devicetype: self.deviceType!)
            walliR.setMacAddress(macAddr: macAddr!)
            walliR.peripheral = BLEManager.getSharedBLEManager().currentPeripheral
            walliR.setDeviceName(devicename: self.deviceName!)
            walliR.setTrackType(tracktype: trackerType!)
            PrefsManager.shared.saveWalli(walliR: walliR)
            PrefsManager.shared.mDeviceMap?[macAddr!] = walliR
            //gotoMainController()
            gotoTutorialScreen()
        }
    }
    @IBAction func onBackPressed() {
        let currentPerihperal = BLEManager.getSharedBLEManager().currentPeripheral!
        if(currentPerihperal.state == CBPeripheralState.connected){
            BLEManager.getSharedBLEManager().disconnectPeripheralDevice(peripheral: currentPerihperal)
        }
        gotoMainController()
    }
    func gotoTutorialScreen() {
        let vc =  self.storyboard?.instantiateViewController(identifier: "tutorialStartViewController") as! TutorialStartViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func gotoMainController(){
        BLEManager.getSharedBLEManager().setRunngMode(mode: .checking)
        self.navigationController?.popToRootViewController(animated: true)
    }
    func unCheckAllItems(){
        ic_checkmark_key.isHidden = true
        ic_checkmark_remote.isHidden = true
        ic_checkmark_luggage.isHidden = true
        ic_checkmark_pet.isHidden = true
        ic_checkmark_bagpack.isHidden = true
        ic_checkmark_camera.isHidden = true
        ic_checkmark_car.isHidden = true
        ic_checkmark_other.isHidden = true
        
    }
    @IBAction func onClickTrackKey() {
        unCheckAllItems()
        ic_checkmark_key.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_key")
        edit_devicename.text = "Key"
        trackerType = 0
    }
    
    @IBAction func onClickTrackRemote(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_remote.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_remote")
        edit_devicename.text = "Remote"
        trackerType = 1

    }
    
    @IBAction func onClickTrackLuggage(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_luggage.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_luggage")
        edit_devicename.text = "Luggage"
        trackerType = 2

    }
    
    @IBAction func onClickTrackPet(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_pet.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_pet")
        edit_devicename.text = "Pet"
        trackerType = 3

    }
    
    @IBAction func onClickBagpack(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_bagpack.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_bagpack")
        edit_devicename.text = "Bagpack"
        trackerType = 4

    }
    @IBAction func onClickCamera(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_camera.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_camera")
        edit_devicename.text = "Camera"
        trackerType = 5

    }
    @IBAction func onClickCar(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_car.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_car")
        edit_devicename.text = "Car"
        trackerType = 6

    }
    @IBAction func onClickOther(_ sender: Any) {
        unCheckAllItems()
        ic_checkmark_other.isHidden = false
        img_tracker_main.image = UIImage(named: "ic_tracker_other")
        edit_devicename.text = "Other"
        trackerType = 7

    }
    
    
}
