//
//  ScanDeviceViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/27/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth

class ScanDeviceViewController: BaseViewController {
    @IBOutlet weak var imgSonar: UIImageView!
    @IBOutlet weak var dottedProgressView: UIView!
    @IBOutlet weak var butConnect: UIButton!
    @IBOutlet weak var labelScanTip: UILabel!
    @IBOutlet weak var labelScanningState: UILabel!

    var dottedProgressBar:DottedProgressBar?
    
    var deviceType: Int? = 0
    var deviceName: String? = nil
    var peripheral: CBPeripheral? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        print("[ScanDeviceView] viewDidLoad")
        appDelegate.svc = self
        self.updateScanningState(state: 0)
        // sonar view
        let sonargif = UIImage.gif(asset: "ic_scan_sonar")
        var imageArray = [UIImage]()
        imageArray.append(UIImage(named: "bluetooth_sonar")!)
        imageArray.append(contentsOf: (sonargif?.images)!)
        self.imgSonar.animationImages = imageArray
        self.imgSonar.animationRepeatCount = 0

        // dotted progressview
        dottedProgressBar = DottedProgressBar()
        dottedProgressBar?.progressAppearance = DottedProgressBar.DottedProgressAppearance(dotRadius: 6.0, dotsColor: UIColor(hexString: "#3333B9BA")!, dotsProgressColor: UIColor(hexString: "#FF33B9BA")!, backColor: UIColor.clear)
        dottedProgressBar?.frame = CGRect(x: 0, y: 0, width: 150, height: 40)
        dottedProgressView.addSubview(dottedProgressBar!)
        dottedProgressBar?.setNumberOfDots(6)
        dottedProgressBar?.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.deviceType = PrefsManager.shared.getDeviceType()
        self.deviceName = PrefsManager.shared.getCurrentDeviceName()
        // change running mode as 'Scanning'
        if self.deviceType != Constant.PRODUCT_TRACKER{
            labelScanTip.text = "Press CONNECT button to start the pairing process"
        }
        else{
            labelScanTip.text = "Press CONNECT button to start the pairing process"
        }
    }

    @IBAction func onClickedConnect(_ sender: Any) {
        let centralManager = BLEManager.getSharedBLEManager().centralManager
        if centralManager?.state != .poweredOn{
            Util.showAlert(vc: self, "Walli", "Please make sure that Bluetooth is turned on.")
        }
        else{
            self.updateScanningState(state: 1)
            self.imgSonar.startAnimating()
            self.dottedProgressBar?.startAnimate()
            BLEManager.getSharedBLEManager().setRunngMode(mode: .scanning)
            self.butConnect.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                BLEManager.getSharedBLEManager().stopScan()
                self.butConnect.isHidden = false
                self.imgSonar.stopAnimating()
                self.dottedProgressBar?.stopAnimate()
                self.updateScanningState(state: 0)
                if(BLEManager.getSharedBLEManager().discoveredBLE!){
                    print("Scanning success.")
                    if(self.deviceType == Constant.PRODUCT_KEYFINDER){
                        self.gotoTrakcerDetailScreen()
                    }
                    else{
                        let macAddr = BLEManager.getSharedBLEManager().currentPeripheral?.identifier.uuidString
                        let walliR = WalliR(devicetype: self.deviceType!)
                        walliR.setMacAddress(macAddr: macAddr!)
                        walliR.peripheral = BLEManager.getSharedBLEManager().currentPeripheral
                        walliR.setDeviceName(devicename: PrefsManager.shared.getProductName())
                        PrefsManager.shared.saveWalli(walliR: walliR)
                        PrefsManager.shared.mDeviceMap?[macAddr!] = walliR
                        BLEManager.getSharedBLEManager().setRunngMode(mode: .checking)
                        //self.onBackPressed()
                        self.gotoTutorialScreen()
                        
                    }

                }
                else{
                    print("Scanning failed.")
                }
            }
        }
    }
    func updateScanningState(state: Int){
        
        if state == 0{
            // normal
            labelScanningState.isHidden = true
            dottedProgressBar?.isHidden = true
        }
        else if state == 1{
            // scanning
            labelScanningState.isHidden = false
            dottedProgressBar?.isHidden = false
            labelScanningState.text = "Scanning..."
        }
        else if state == 2{
            //connecting
            labelScanningState.isHidden = false
            dottedProgressBar?.isHidden = false
            labelScanningState.text = "Connecting..."
        }
    }
    func onBackPressed(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func gotoTutorialScreen() {
        let vc =  self.storyboard?.instantiateViewController(identifier: "tutorialStartViewController") as! TutorialStartViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func gotoTrakcerDetailScreen(){
        let vc =  self.storyboard?.instantiateViewController(identifier: "trackerDetailViewController") as! TrackerDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
