//
//  SettingsViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/28/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import CoreBluetooth


class SettingsViewController: BaseViewController, UIPopoverPresentationControllerDelegate, SSRadioButtonControllerDelegate {

    @IBOutlet weak var radioNear: UIButton!
    @IBOutlet weak var radioModerate: UIButton!
    @IBOutlet weak var radioFar: UIButton!
    
    @IBOutlet weak var radioOn: UIButton!
    @IBOutlet weak var radioMute: UIButton!
    @IBOutlet weak var radioOff: UIButton!
    
    @IBOutlet weak var imgBatteryLevel: UIImageView!

    @IBOutlet weak var labelCardNotificationDelay: UILabel!
    @IBOutlet weak var butRange: UIButton!
    @IBOutlet weak var butCardNotifyTip: UIButton!
    
    @IBOutlet weak var cardNotificationView: UIView!
    @IBOutlet weak var cardNotificationHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emergencySwitch: UISwitch!
            
    @IBOutlet weak var personalSafetyView: UIView!
    
    var rangeButController: SSRadioButtonsController?
    var alarmButController: SSRadioButtonsController?
    var walliR: WalliR? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        radioNear?.setTitleColor(UIColor.black, for: UIControl.State.selected)
        radioModerate?.setTitleColor(UIColor.black, for: UIControl.State.selected)
        radioFar?.setTitleColor(UIColor.black, for: UIControl.State.selected)
        
        rangeButController = SSRadioButtonsController(buttons: radioNear, radioModerate, radioFar)
        rangeButController?.delegate = self
        rangeButController?.shouldLetDeSelect = true
        
        radioOn.setTitleColor(UIColor.black, for: UIControl.State.selected)
        radioMute.setTitleColor(UIColor.black, for: UIControl.State.selected)
        radioOff.setTitleColor(UIColor.black, for: UIControl.State.selected)

        alarmButController = SSRadioButtonsController(buttons: radioOn, radioMute, radioOff)
        alarmButController?.delegate = self
        alarmButController?.shouldLetDeSelect = true
    }
    override func viewWillAppear(_ animated: Bool) {
        print("[Settings] viewWillAppear")
        loadSetting()
    }
    func loadSetting(){
        if(walliR == nil){
            return
        }
        if(walliR?.devicetype == Constant.PRODUCT_TRACKER){
            cardNotificationView.alpha = 0
            cardNotificationHeightConstraint.constant = 0
            self.personalSafetyView.isHidden = false
        }
        else{
            self.personalSafetyView.isHidden = true
        }
        if(walliR?.rangemode == WalliR.RANGE_NEAR){
            radioNear.isSelected = true
            radioModerate.isSelected = false
            radioFar.isSelected = false
        }
        else if(walliR?.rangemode == WalliR.RANGE_MODERATE){
            radioNear.isSelected = false
            radioModerate.isSelected = true
            radioFar.isSelected = false
        }
        else if(walliR?.rangemode == WalliR.RANGE_FAR){
            radioNear.isSelected = false
            radioModerate.isSelected = false
            radioFar.isSelected = true
        }
        if(walliR?.alarmmode == WalliR.SEPARATION_ALARM_ON){
            radioOn.isSelected = true
            radioMute.isSelected = false
            radioOff.isSelected = false
        }
        else if(walliR?.alarmmode == WalliR.SEPARATION_ALARM_MUTE){
            radioOn.isSelected = false
            radioMute.isSelected = true
            radioOff.isSelected = false
        }
        else if(walliR?.alarmmode == WalliR.SEPARATION_ALARM_OFF){
            radioOn.isSelected = false
            radioMute.isSelected = false
            radioOff.isSelected = true
        }

        labelCardNotificationDelay.text = String(walliR?.cardnotificationdelay ?? 10) + "s"
        displayBatteryLevel()
        
        let emergencyMode = PrefsManager.shared.getEmergencyContactMode()
        refreshEmergencyContact(mode: emergencyMode)

    }
    func refreshEmergencyContact(mode: Bool){
        if(mode){
            emergencySwitch.setOn(true, animated: false)
        }
        else{
            emergencySwitch.setOn(false, animated: false)
        }
    }
    func displayBatteryLevel(){
        guard let batteryLevel = walliR?.battery else { return }
        if(batteryLevel>=95){
            imgBatteryLevel.image = UIImage(named: "ic_battery_100");
        }
        else if(batteryLevel>=85){
           imgBatteryLevel.image = UIImage(named: "ic_battery_90");
        }
        else if(batteryLevel>=75){
           imgBatteryLevel.image = UIImage(named: "ic_battery_80");
        }
        else if(batteryLevel>=65){
            imgBatteryLevel.image = UIImage(named: "ic_battery_70");
        }
        else if(batteryLevel>=55){
            imgBatteryLevel.image = UIImage(named: "ic_battery_60");
        }
        else if(batteryLevel>=45){
            imgBatteryLevel.image = UIImage(named: "ic_battery_50");
        }
        else if(batteryLevel>=35){
            imgBatteryLevel.image = UIImage(named: "ic_battery_40");
        }
        else if(batteryLevel>=25){
            imgBatteryLevel.image = UIImage(named: "ic_battery_30");
        }
        else if(batteryLevel>=15){
            imgBatteryLevel.image = UIImage(named: "ic_battery_20");
        }
        else if(batteryLevel>=5){
            imgBatteryLevel.image = UIImage(named: "ic_battery_10");
        }
        else{
            imgBatteryLevel.image = UIImage(named: "ic_battery_0");
        }
    }
    func setWalli(walliR: WalliR){
        print("[Settings] Walli Information Setted")
        self.walliR = walliR
    }
    func didSelectButton(selectedButton: UIButton?) {
        if(selectedButton == radioNear){
            walliR?.rangemode = WalliR.RANGE_NEAR
        }
        else if(selectedButton == radioModerate){
            walliR?.rangemode = WalliR.RANGE_MODERATE
        }
        else if(selectedButton == radioFar){
            walliR?.rangemode = WalliR.RANGE_FAR
        }
        else if(selectedButton == radioOff){
            walliR?.alarmmode = WalliR.SEPARATION_ALARM_OFF
        }
        else if(selectedButton == radioMute){
            walliR?.alarmmode = WalliR.SEPARATION_ALARM_MUTE
        }
        else if(selectedButton == radioOn){
            walliR?.alarmmode = WalliR.SEPARATION_ALARM_ON
        }
        PrefsManager.shared.updateWalli(walliR: walliR)
    }
    @IBAction func onTipRange(_ sender: UIButton) {
        showToolTip(tip: Constant.TIP_RANGE, view: sender)

    }
    @IBAction func onTipNotifyCard(_ sender: UIButton) {
        showToolTip(tip: Constant.TIP_CARDNOTIFY, view: sender)
    }
    
    @IBAction func onTipSeparationAlarm(_ sender: UIButton) {
        showToolTip(tip: Constant.TIP_SEPARATION_ALARM, view: sender)
    }
    
    @IBAction func onTipPersonalSafetyButton(_ sender: UIButton) {
        showToolTip(tip: Constant.TIP_EMERGENCY, view: sender)
    }
    
    @IBAction func onPressCardNotificationDelaySetting(_ sender: Any) {
        PickerDialog().show(title: "Set Delay", min: 0, max: 60, selected: walliR!.cardnotificationdelay) {
            (value) -> Void in
            self.walliR?.cardnotificationdelay = value
            PrefsManager.shared.updateWalli(walliR: self.walliR)
            self.loadSetting()
        }
    }
    func showToolTip(tip: String, view: UIView){
        let vc =  self.storyboard?.instantiateViewController(identifier: "popupViewController") as! PopupViewController
        vc.modalPresentationStyle = .popover
        
        if let popover = vc.popoverPresentationController {

           let viewForSource = view
           popover.sourceView = viewForSource

           // the position of the popover where it's showed
           popover.sourceRect = viewForSource.bounds
            
           let labelSize = tip.size(withAttributes: [.font: UIFont.systemFont(ofSize: 14)])
          
            vc.preferredContentSize = CGSize(width: labelSize.width, height: labelSize.height + 20)
           // the size you want to display
            vc.setTipText(tip: tip)
           popover.delegate = self
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @IBAction func onPressedSafetyEmergency(_ sender: Any) {
        let emergencyMode = PrefsManager.shared.getEmergencyContactMode()
        if emergencyMode == true {
            self.launchSafetyEmergencySetup()
        }
    }
    
    @IBAction func onSwitchEmergencyContact(_ sender: Any) {
        PrefsManager.shared.saveEmergencyContact(mode: emergencySwitch.isOn)
        refreshEmergencyContact(mode: emergencySwitch.isOn)
        if(emergencySwitch.isOn) {
            self.launchSafetyEmergencySetup()
        }
    }

    func launchSafetyEmergencySetup() {
        let vc =  self.storyboard?.instantiateViewController(identifier: "safetyPermissionViewController") as! SafetyPermissionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: 75, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    @IBAction func onSetMyNamePressed(_ sender: Any) {

    }
    
    @IBAction func onContact1Pressed(_ sender: Any) {

    }
    
    
    @IBAction func onContact2Pressed(_ sender: Any) {


    }
    
    @IBAction func onPressDisconnect(_ sender: Any) {
        //if(walliR?.peripheral?.state == CBPeripheralState.connected){
            if walliR?.peripheral != nil {
                BLEManager.getSharedBLEManager().disconnectPeripheralDevice(peripheral: (walliR?.peripheral)!)
            }
            PrefsManager.shared.deleteWalli(walliR: walliR!)
            
            self.navigationController?.popViewController(animated: true)
        //}
    }
    
}
