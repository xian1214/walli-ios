//
//  WifiSettingsViewController.swift
//  Walli
//
//  Created by Xian Huang on 2/3/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class WifiSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    var wifiList: [String] = []
    @IBOutlet weak var wifiTableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wifiList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! WifiItemCell
        cell.butSelected.isSelected = !cell.butSelected.isSelected
        if cell.butSelected.isSelected{
            PrefsManager.shared.saveHomeWifiSSID(ssid: cell.labelWifiName.text!)
            addUsedWifiList(item: cell.labelWifiName.text!)
        }
        else{
            PrefsManager.shared.saveHomeWifiSSID(ssid: "")
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "_WifiItemCell", for: indexPath) as! WifiItemCell
        cell.labelWifiName.text = wifiList[indexPath.row]
        
        if cell.labelWifiName.text == PrefsManager.shared.getHomeWifiSSID(){
            cell.butSelected.isSelected = true
        }
        else{
            cell.butSelected.isSelected = false
        }
        return cell
    }
    func addUsedWifiList(item: String){
        var isExistSSID = false
        for ssid in wifiList {
            if item == ssid{
                isExistSSID = true
                break
            }
        }
        if isExistSSID == false{
            wifiList.append(item)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        wifiTableView.delegate = self
        wifiTableView.dataSource = self
        wifiTableView.register(UINib(nibName: "WifiItemCell", bundle: nil), forCellReuseIdentifier: "_WifiItemCell")
        wifiList = PrefsManager.shared.loadUsedWifiList()
        guard let currentSSID = WifiUtil.fetchCurrentSSIDInfo()  else { return }
        
        addUsedWifiList(item: currentSSID)
        wifiTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
