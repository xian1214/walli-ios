//
//  WifiItemCell.swift
//  Walli
//
//  Created by Xian Huang on 2/3/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class WifiItemCell: UITableViewCell {

    @IBOutlet weak var labelWifiName: UILabel!
    @IBOutlet weak var butSelected: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
