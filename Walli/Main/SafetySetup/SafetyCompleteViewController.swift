//
//  SafetyCompleteViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class SafetyCompleteViewController: UIViewController {

    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.descriptionLabel.setLineHeight(lineHeight: 6)
        // Do any additional setup after loading the view.
        let deviceType = PrefsManager.shared.getDeviceType()
        if deviceType == Constant.PRODUCT_TRACKER {
            self.descriptionLabel.text = "Enjoy being protected with the Trail!"
        }
    }
    

    @IBAction func onPressedNext(_ sender: Any) {
        for vc in (self.navigationController?.viewControllers ?? []) {
            if vc is SettingsViewController {
                _ = self.navigationController?.popToViewController(vc, animated: true)
                break
            }
        }
    }
    

}
