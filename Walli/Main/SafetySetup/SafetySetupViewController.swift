//
//  SafetySetupViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import ContactsUI
class SafetySetupViewController: UIViewController, CNContactPickerDelegate {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var fullNameButton: UIButton!
    
    @IBOutlet weak var contact1Button: UIButton!
    
    @IBOutlet weak var contact2Button: UIButton!
    
    var contactNumber: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.descriptionLabel.setLineHeight(lineHeight: 6)
        // Do any additional setup after loading the view.
        self.refreshEmergencyContact()
    }
    
    
    @IBAction func onPressedFullName(_ sender: Any) {
        let alert = UIAlertController(title: "Please enter your full name.", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Your name..."
        })

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

            if let code = alert.textFields?.first?.text {
                PrefsManager.shared.saveContactMyName(name: code)
                self.refreshEmergencyContact()
            }
        }))

        self.present(alert, animated: true)
    }
    
    
    @IBAction func onPressedEmergencyContact1(_ sender: Any) {
        contactNumber = 0
        let contactName1 = PrefsManager.shared.getContactName1()
        let contactNumber1 = PrefsManager.shared.getContactNumber1()
        if(contactName1 == "" || contactNumber1 == ""){
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self
            contactPicker.displayedPropertyKeys =
                [CNContactGivenNameKey
                    , CNContactPhoneNumbersKey]
            self.present(contactPicker, animated: true, completion: nil)
        }
        else{
            PrefsManager.shared.saveContactName1(name: "")
            PrefsManager.shared.saveContactNumber1(name: "")
            refreshEmergencyContact()
        }
    }
    
    @IBAction func onPressedEmergencyContact2(_ sender: Any) {
        contactNumber = 1
        let contactName2 = PrefsManager.shared.getContactName2()
        let contactNumber2 = PrefsManager.shared.getContactNumber2()
        if(contactName2 == "" || contactNumber2 == ""){
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self
            contactPicker.displayedPropertyKeys =
                [CNContactGivenNameKey
                    , CNContactPhoneNumbersKey]
            self.present(contactPicker, animated: true, completion: nil)
        }
        else{
            PrefsManager.shared.saveContactName2(name: "")
            PrefsManager.shared.saveContactNumber2(name: "")
            refreshEmergencyContact()
        }
    }

    @IBAction func onPressedNext(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "safetyUsageViewController") as! SafetyUsageViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    func refreshEmergencyContact(){
        let contactMyName = PrefsManager.shared.getContactMyName()
        fullNameButton.setTitle(contactMyName == "" ? "Enter Your Full Name" : contactMyName, for: .normal)
        
        let contactName1 = PrefsManager.shared.getContactName1()
        contact1Button.setTitle(contactName1 == "" ? "Pick Emergency Contact #1" : contactName1, for: .normal)
        
        let contactName2 = PrefsManager.shared.getContactName2()
        contact2Button.setTitle(contactName2 == "" ? "Pick Emergency Contact #2" : contactName2, for: .normal)
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // You can fetch selected name and number in the following way

        // user name
        let userName:String = contact.givenName

        // user phone number
        //let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        //let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        let firstPhoneNumber = contact.phoneNumbers.first


        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber?.value.stringValue ?? ""

        if(contactNumber == 0){
            PrefsManager.shared.saveContactName1(name: userName)
            PrefsManager.shared.saveContactNumber1(name: primaryPhoneNumberStr)
        }
        else{
            PrefsManager.shared.saveContactName2(name: userName)
            PrefsManager.shared.saveContactNumber2(name: primaryPhoneNumberStr)
        }
//        showToast(message: "Phone number: \(primaryPhoneNumberStr)", font: .systemFont(ofSize: 12.0))
        refreshEmergencyContact()
        print(userName)
        print(primaryPhoneNumberStr)

    }
}
