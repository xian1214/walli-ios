//
//  SafetyPermissionViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class SafetyPermissionViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var contactView: ExtensionView!
    
    @IBOutlet weak var contactButton: UIButton!
    
    @IBOutlet weak var contactImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.descriptionLabel.setLineHeight(lineHeight: 6)
        if(checkContactPermission()){
            contactView.backgroundColor = UIColor(hexString: "#35ADB0")
            contactButton.setTitleColor(UIColor.white, for: .normal)
            contactImageView.image = UIImage(named: "ic_permission_checkmark")
        }
        else{
            contactView.backgroundColor = UIColor.clear
            contactButton.setTitleColor(UIColor(hexString: "#35ADB0"), for: .normal)
            contactImageView.image = UIImage(named: "ic_safety_contacts")
        }
    }
    func checkContactPermission() -> Bool {
        return true
    }
    @IBAction func onPressedContactPermission(_ sender: Any) {
        
    }
    
    @IBAction func onPressedNext(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "safetySetupViewController") as! SafetySetupViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}
