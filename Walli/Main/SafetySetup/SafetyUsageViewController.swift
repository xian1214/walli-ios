//
//  SafetyUsageViewController.swift
//  Walli
//
//  Created by devstar on 10/8/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class SafetyUsageViewController: UIViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var sendSMSImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.descriptionLabel.setLineHeight(lineHeight: 6)
        let logoGif = UIImage.gif(asset: "ic_pebble_press_two")
        self.sendSMSImageView.animationImages = logoGif?.images
        self.sendSMSImageView.image = logoGif?.images![(logoGif?.images!.count)!-1]
//            self.usageImageView.animationRepeatCount = 1
        self.sendSMSImageView.startAnimating()
    }
    

    @IBAction func onPressedNext(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "safetyCompleteViewController") as! SafetyCompleteViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    

}
