//
//  GeneralSettingViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/26/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class GeneralSettingViewController: BaseViewController {

    @IBOutlet weak var butWifiSilentZone: UIButton!
    @IBOutlet weak var butBackPacketMode: UIButton!
    @IBOutlet weak var labelWifiSSID: UILabel!
    
    
    var isWifiSilentZone: Bool? = false
    var isBackPacketMode: Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        loadGeneralSetting()
    }
    func loadGeneralSetting(){
        self.isWifiSilentZone = PrefsManager.shared.getWifiSilentZone()
        self.isBackPacketMode = PrefsManager.shared.getBackPacketMode()
        if self.isWifiSilentZone! == false{
            let image = UIImage(named: "ic_switch_off")
            butWifiSilentZone.setImage(image, for: .normal)
            labelWifiSSID.text = "Select your home Wifi"
        }
        else{
            let image = UIImage(named: "ic_switch_on")
            butWifiSilentZone.setImage(image, for: .normal)
            let currentSSID = PrefsManager.shared.getHomeWifiSSID()
            if currentSSID.isEmpty{
                labelWifiSSID.text = "Select your home Wifi"
            }
            else{
                labelWifiSSID.text = "Current Wifi: " + currentSSID
            }
        }
        if self.isBackPacketMode! == false{
            let image = UIImage(named: "ic_switch_off")
            butBackPacketMode.setImage(image, for: .normal)
        }
        else{
            let image = UIImage(named: "ic_switch_on")
            butBackPacketMode.setImage(image, for: .normal)
        }
    }
    
    @IBAction func onClickSupport(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "aboutUsViewController") as! AbousUsViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func onTouchWifiSilentZone(_ sender: Any) {
        let vc =  self.storyboard?.instantiateViewController(identifier: "wifiSettingsViewController") as! WifiSettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onTouchWifiSilentZoneMode(_ sender: Any) {
        self.isWifiSilentZone = !self.isWifiSilentZone!
        PrefsManager.shared.saveWifiSilentZone(mode: self.isWifiSilentZone!)
        loadGeneralSetting()
        
    }
    
    @IBAction func onTouchBackPacketMode(_ sender: Any) {
        self.isBackPacketMode = !self.isBackPacketMode!
        if self.isBackPacketMode == true{
            Util.showAlert(vc: self, "Back Pocket Mode", "When this feature is enabled, you will have to double tap TWICE (4 taps total) on your wallet in order to ring your phone.\n\nThis also disables sound notification for lost wallet but you will still receive a visual alert. This will not disable the sound for lost card.")
        }
        PrefsManager.shared.saveBackPacketMode(mode: self.isBackPacketMode!)
        loadGeneralSetting()

    }
    

}
