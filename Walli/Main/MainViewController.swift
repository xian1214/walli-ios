//
//  MainViewController.swift
//  Walli
//
//  Created by Xian Huang on 1/20/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var navigationDrawer: UITableView!
    @IBOutlet weak var navigationDrawerLeading: NSLayoutConstraint!

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var maskView: UIView!
    
    var childVC: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("MainViewController DidLoad...")
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        print("MainViewController WillAppear...")
        
        if PrefsManager.shared.getDeviceCount() == 0{
            self.setContentView(storyboardId: "contentEmptyViewController")
        }
        else{            
            self.setContentView(storyboardId: "contentMainViewController")
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        let frm = navigationDrawer.frame
        navigationDrawerLeading.constant = -frm.width
                
    }
    func setContentView( storyboardId: String ) {
        let height = self.contentView.frame.height
        
        self.childVC?.view.removeFromSuperview()
        self.childVC?.removeFromParent()
        
//        self.childVC = UIStoryboard(name: storyboardId, bundle: nil).instantiateInitialViewController()
        self.childVC = self.storyboard?.instantiateViewController(identifier: storyboardId)
        if self.childVC == nil {
            return
        }
        
        self.childVC!.view.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: height)
        
        self.addChild(self.childVC!)
        self.contentView.addSubview(self.childVC!.view)
    }
    func setupView(){
        navigationDrawer.delegate = self
        navigationDrawer.dataSource = self
        
        navigationDrawer.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: "_HeaderCell")
        navigationDrawer.register(UINib(nibName: "ItemCell", bundle: nil), forCellReuseIdentifier: "_ItemCell")
        // gesture handler
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        maskView.addGestureRecognizer(tap)
        maskView.alpha = 0

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        CloseNavigationDrawer()
    }

    @IBAction func onBtnMenu(_ sender: Any) {
        showMenu()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "_HeaderCell", for: indexPath) as! HeaderCell
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "_ItemCell", for: indexPath) as! ItemCell
        
        if(indexPath.row == 1){
            cell.menuImage.image = UIImage(named: "nav_adddevice")
            cell.menuText.text = "Add Device"

        }
        else if(indexPath.row == 2){
            cell.menuImage.image = UIImage(named: "nav_setting")
            cell.menuText.text = "General Setting"
        }
        else if(indexPath.row == 3){
            cell.menuImage.image = UIImage(named: "nav_about")
            cell.menuText.text = "About"

        }

        cell.selectionStyle = .none
        return cell
    }
/*
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != 0 {
            return 57
        }
        
    }
*/
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 1)
        {
            // Launch AddDeviceController
            pushViewController(storyboardId: "selectProductsViewController")
            CloseNavigationDrawer()
        }
        else if (indexPath.row == 2)
        {
            // Push GeneralSettingControlller
            pushViewController(storyboardId: "generalSettingController")
            CloseNavigationDrawer()
        }
        else if (indexPath.row == 3)
        {
            // Launch AbousUsController
            pushViewController(storyboardId: "aboutUsViewController")
            CloseNavigationDrawer()
        }
        
    }
    func pushViewController(storyboardId: String)
    {
        let vc =  self.storyboard?.instantiateViewController(identifier: storyboardId)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func CloseNavigationDrawer()
    {
        hideMenu()
    }
    func showMenu() {
        
        navigationDrawerLeading.constant = 0
        UIApplication.shared.windows.first?.windowLevel = UIWindow.Level.statusBar
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            self.maskView.alpha = 0.7
         })
     }
     
     func hideMenu() {
         navigationDrawerLeading.constant = -navigationDrawer.frame.width
         UIApplication.shared.windows.first?.windowLevel = UIWindow.Level.normal

         UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
            self.maskView.alpha = 0
         })
    }

}

