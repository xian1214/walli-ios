//
//  BLEUtility.swift
//  Walli
//
//  Created by Xian Huang on 2/2/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation
import CoreBluetooth
class BLEUtility{
    
    static func writeCharacteristic(peripheral: CBPeripheral, suuid: CBUUID, cuuid: CBUUID, data: Data) -> Bool{
        guard let services = peripheral.services else { return false}
        for service in services{
            if service.uuid == suuid{
                guard let characteristics = service.characteristics else { return false }
                for characteristic in characteristics{
                    if characteristic.uuid == cuuid{
                        if characteristic.properties.rawValue ==  CBCharacteristicProperties.read.rawValue|CBCharacteristicProperties.write.rawValue{
                            peripheral.writeValue(data, for: characteristic, type: .withResponse)
                        }
                        else{
                            peripheral.writeValue(data, for: characteristic, type: .withoutResponse)
                        }
                        return true
                    }
                }
            }
        }
        return false
    }
    static func readCharacteristic(peripheral: CBPeripheral, suuid: CBUUID, cuuid: CBUUID){
        guard let services = peripheral.services else { return }
        for service in services{
            if service.uuid == suuid{
                guard let characteristics = service.characteristics else { break }
                for characteristic in characteristics{
                    if characteristic.uuid == cuuid{
                        peripheral.readValue(for: characteristic)
                    }
                }
            }
        }
    }
}
