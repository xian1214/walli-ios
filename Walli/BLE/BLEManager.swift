//
//  BLEManager.swift
//
//  Created by IQVIS  on 10/24/16.
//  Copyright © 2016 IQVIS. All rights reserved.
/*
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License. */

import UIKit
import Foundation
import CoreBluetooth

private var sharedBLEManager: BLEManager? = nil

enum RunningMode {
    case scanning
    case checking
    case lostcode
}

class BLEManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var centralManager: CBCentralManager? = nil
    var runningMode: RunningMode? = .none
    var peripherals: [CBPeripheral] = []
    var currentPeripheral: CBPeripheral? = nil
    var discoveredBLE: Bool? = false
    weak var stopScanTimer: Timer?
    var taskId: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    var timer: Timer?
    var walliPrefix: String? = nil
    static func getSharedBLEManager () -> BLEManager {
        if sharedBLEManager == nil {
            sharedBLEManager = BLEManager()
        }
        return sharedBLEManager!
    }
   // MARK: Intializing BLE CentralManager
    func initCentralManager() {
        let concurrentQueue = DispatchQueue(label: "walliCMQueue")
//        concurrentQueue.sync {
//            print("sync task")
//        }
//        if self.centralManager == nil {
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: [CBCentralManagerOptionShowPowerAlertKey: true, CBCentralManagerOptionRestoreIdentifierKey: "WalliCentralManagerIdentifier"])
//        }

        /*
        if timer != nil{
            timer?.invalidate()
        }
        taskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.taskId)
        })
        print("shcedule timer started...")
        timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.timerTask), userInfo: nil, repeats: true)
 */

    }
    func setWalliPrefix(prefix: String){
        self.walliPrefix = prefix
    }
    @objc func timerTask(){
//        print("update on \(Date())")
        checkScanInBackground()
    }
    func stopTimer(){
        timer?.invalidate()
    }
    // check if the bluetooth is on
    func checkBluetoothOn()->Bool{
        if self.centralManager != nil{
            if self.centralManager!.state != CBManagerState.poweredOn{
                initCentralManager()
                return false;
            }
            checkConnect()
            return true
        }
        return false;
    }
    func checkScanInBackground(){
        if self.centralManager != nil && self.centralManager!.state == CBManagerState.poweredOn{
            if self.runningMode == .checking && self.centralManager?.isScanning == false{
                peripherals.removeAll()
                scanDevice(serviceUUIDs: [CBUUID.IMMEDIATE_ALERT_SERVICE_UUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
            }
        }
    }
    func checkConnect() -> Bool{
        var isIsconnected = true
        let deviceMap = Array(PrefsManager.shared.getDeviceMap().values)
        for walliR in deviceMap{
            if walliR.peripheral == nil || walliR.peripheral?.state == CBPeripheralState.disconnected{
                isIsconnected = false
                break;
            }
        }
        if !isIsconnected{
            print("[BLEManager] Current State: Disconnected.")
            setRunngMode(mode: .checking)
        }
        
        return isIsconnected;
    }
    func setRunngMode(mode: RunningMode){
        self.runningMode = mode
        self.discoveredBLE = false
        self.stopScan()
        peripherals.removeAll()
        scanDevice(serviceUUIDs: [CBUUID.IMMEDIATE_ALERT_SERVICE_UUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }
    func sendRingStart(peripheral: CBPeripheral){
        let verifyValue: [UInt8] = [0x02]
        let verifyData = Data(bytes: verifyValue, count: 1)
        let bRet = BLEUtility.writeCharacteristic(peripheral: peripheral, suuid: CBUUID.IMMEDIATE_ALERT_SERVICE_UUID, cuuid: CBUUID.CHARACTERISTIC_ALERT_LEVEL_UUID, data: verifyData)
        if(bRet){
            appDelegate.cvc?.updateUIForRing(peripheral: peripheral, bRingStart: true)
        }
    }
    func sendRingStop(peripheral: CBPeripheral){
        let verifyValue: [UInt8] = [0x00]
        let verifyData = Data(bytes: verifyValue, count: 1)
        let bRet = BLEUtility.writeCharacteristic(peripheral: peripheral, suuid: CBUUID.IMMEDIATE_ALERT_SERVICE_UUID, cuuid: CBUUID.CHARACTERISTIC_ALERT_LEVEL_UUID, data: verifyData)
        if(bRet){
            appDelegate.cvc?.updateUIForRing(peripheral: peripheral, bRingStart: false)
        }
    }
    func readBatteryStatus(peripheral: CBPeripheral){
        BLEUtility.readCharacteristic(peripheral: peripheral, suuid: CBUUID.BATTERY_SERVICE_UUID, cuuid: CBUUID.CHARACTERISTIC_BATTERY_LEVEL_UUID)
    }
    // MARK: BLE CBCentralManagerDelegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if #available(iOS 10.0, *) {
            switch central.state {
            case CBManagerState.unauthorized:
                break
            case CBManagerState.poweredOff:
                print("Device State Changed: Power Off")
                NotificationUtil.generateNotification(type: .NOTIFICATION_ID_BLUETOOTHOFF, walliR: nil)
                appDelegate.cvc?.updateUIForReload()
                break
            case CBManagerState.resetting:
                break
            case CBManagerState.poweredOn:
                print("Device State Changed: Power On")
                self.checkConnect()
                break
            default:
                break
            }
        } else {

           let cmState: CBCentralManagerState = (CBCentralManagerState)(rawValue: central.state.rawValue)!
            switch cmState {
            case CBCentralManagerState.unauthorized:
                break
            case CBCentralManagerState.poweredOff:
                NotificationUtil.generateNotification(type: .NOTIFICATION_ID_BLUETOOTHOFF, walliR: nil)
                appDelegate.cvc?.updateUIForReload()
                break
            case CBCentralManagerState.poweredOn:
                self.checkConnect()
                break
            case CBCentralManagerState.resetting:
                break
            default:
                break
            }
        }
        appDelegate.cpvc?.refreshStatus()
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        if let peripherals:[CBPeripheral] = dict[CBCentralManagerRestoredStatePeripheralsKey] as! [CBPeripheral]? {
            for peripheral in peripherals{
                if PrefsManager.shared.containsPeripheral(peripheral: peripheral){
                    peripheral.delegate = self
                    if peripheral.state == CBPeripheralState.disconnected{
                        self.connectPeripheralDevice(peripheral: peripheral, options: nil)
                    }
                    else if peripheral.state == CBPeripheralState.connected{
                        let deviceMap = PrefsManager.shared.getDeviceMap()
                        deviceMap[peripheral.identifier.uuidString]?.peripheral = peripheral
                        appDelegate.cvc?.updateUIForConnected(peripheral: peripheral, state: true)
                    }
                    print("[BLEManager] willRestoreState 111 \(peripheral.name), \(peripheral.state.rawValue)")
                }
                else{
                    print("[BLEManager] willRestoreState 222 \(peripheral.name), \(peripheral.state.rawValue)")
                    disconnectPeripheralDevice(peripheral: peripheral)
                }
            }
        }

    }
 
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        peripheral.delegate = self
        /*
        for (index, foundPeripheral) in peripherals.enumerated() {
            if foundPeripheral.peripheral?.identifier == peripheral.identifier {
                peripherals[index].lastRSSI = RSSI
                return
            }
        }
 */
        let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as? Bool
        let localName = advertisementData["kCBAdvDataLocalName"] as? String
        if isConnectable! {
            var exist = false
            for (index, foundPeripheral) in peripherals.enumerated() {
                if foundPeripheral.identifier == peripheral.identifier {
                    exist = true
                }
            }
            if(!exist){
                peripherals.append(peripheral)
            }
//            print("Peripheral discovered...\(peripheral.name)")

            if self.runningMode == .scanning{
                let comp = peripheral.name! as NSString
                let currentDeviceName = PrefsManager.shared.getCurrentDeviceName()
                if comp.contains(currentDeviceName){
                    stopScan()
                    // establish connection
                    if peripheral.state == CBPeripheralState.disconnected{
                        print("Start connecting to device...")
                        appDelegate.svc?.updateScanningState(state: 2)
                        connectPeripheralDevice(peripheral: peripheral, options: nil)
                    }
                }
            }
            else if self.runningMode == .lostcode{
                let comp = peripheral.name! as NSString
//                print("Scanning device: \(comp), \(self.walliPrefix)")
                if comp.contains(self.walliPrefix!) == true {
                    stopScan()
                    let walliCode = comp.replacingOccurrences(of: self.walliPrefix!, with: "")
                    appDelegate.lvc?.updateWalliCode(walliCode: walliCode)
                }
            }
            else{
                let deviceMap = Array(PrefsManager.shared.getDeviceMap().values)
                for walliR in deviceMap{
                    if walliR.macaddress == peripheral.identifier.uuidString{
                        print("\(peripheral.name) Status [\(peripheral.state.rawValue)]")
                        if peripheral.state == CBPeripheralState.disconnected{
                            self.connectPeripheralDevice(peripheral: peripheral, options: nil)
                        }
                    }

                }
            }
        }
    }
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        print("Device Paired Successfuly")
        // start discover periperal
        discoverAllServices(peripheral: peripheral)
    }
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Device Paired Failed")
        appDelegate.cvc?.updateUIForConnected(peripheral: peripheral, state: false)
    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Device Unpaired Successfuly")
        appDelegate.cvc?.updateUIForConnected(peripheral: peripheral, state: false)
        setRunngMode(mode: .checking)
        
    }
    // MARK: BLE CBPeripheralDelegate

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("didDiscoverServices.")
        peripheral.services?.forEach({(service) in
            discoverAllCharacteristics(peripheral: peripheral, service: service)
        })
        // discovered Service
        currentPeripheral = peripheral

        if self.runningMode == .scanning{
            discoveredBLE = true
        }
        else{
            let deviceMap = PrefsManager.shared.getDeviceMap()
            deviceMap[peripheral.identifier.uuidString]?.peripheral = peripheral
            appDelegate.cvc?.updateUIForConnected(peripheral: peripheral, state: true)
            discoveredBLE = false
        }
    }
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("didDiscoverCharacteristicsFor service")
        let verifyValue: [UInt8] = [0xA1, 0xA2, 0xA3, 0xA4]
        let verifyData = Data(bytes: verifyValue, count: 4)
        service.characteristics?.forEach({(characteristic) in
//            discoverDescriptorsByCharacteristic(peripheral: peripheral, characteristic: characteristic)
            if characteristic.uuid == CBUUID.CHARACTERISTIC_CUSTOM_VERIFIED{
                setNotifyValue(peripheral: peripheral, enabled: true, char: characteristic)
                writeCharacteristicValue(peripheral: peripheral, data: verifyData, char: characteristic, type: .withResponse)
            }
            else if characteristic.uuid == CBUUID.CHARACTERISTIC_KEY_PRESS_UUID || characteristic.uuid == CBUUID.CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID{
                setNotifyValue(peripheral: peripheral, enabled: true, char: characteristic)
            }
        })
    }
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        print("didDiscoverDescriptorsFor chracteristic")
    }
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateValueFor chracteristic")
        appDelegate.cvc?.updateUIForCharacteristic(peripheral: peripheral, characteristic: characteristic)
        appDelegate.pvc?.updateUIForCharacteristic(peripheral: peripheral, characteristic: characteristic)
    }
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didWriteValueFor chracteristic")
    }
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateNotificationStateFor chracteristic")
    }
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        print("didReadRSSI RSSI")
    }
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        print("didWriteValueFor descriptor")
    }
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        print("didUpdateValueFor descriptor")
    }
    // MARK: Scanning Process
    func scanAllDevices() {
        self.scanDevice(serviceUUIDs: nil, options: nil)
    }
    func scanDevice(serviceUUIDs: [CBUUID]?, options: [String : Any]?) {
        if stopScanTimer != nil {
            stopScanTimer?.invalidate()
            stopScanTimer = nil
        }
        print("Scanning Device...")
        centralManager?.scanForPeripherals(withServices: serviceUUIDs, options: options)
        if(runningMode != .checking){
            self.applyStopScanTimer()
        }
    }
    func stopScan() {
        centralManager?.stopScan()
    }
    @objc func stopScanningAfterInterval() {
            if self.centralManager!.isScanning {
                self.centralManager?.stopScan()
            }
    }
    // MARK: Connection Process
    func connectPeripheralDevice(peripheral: CBPeripheral, options: [String : Any]?) {
        self.centralManager?.connect(peripheral, options: options)
    }
    func disconnectPeripheralDevice(peripheral: CBPeripheral) {
            self.centralManager?.cancelPeripheralConnection(peripheral)
    }
    // MARK: Discover Services
    func discoverAllServices(peripheral: CBPeripheral) {
        peripheral.discoverServices(nil)
    }
    func discoverServiceByUUIDs(servicesUUIDs: NSArray, peripheral: CBPeripheral) {
        peripheral.discoverServices(servicesUUIDs as? [CBUUID])
    }
    // MARK: Discover Characteristics
    func discoverAllCharacteristics(peripheral: CBPeripheral, service: CBService) {
        peripheral.discoverCharacteristics(nil, for: service)
    }
    func discoverCharacteristicsByUUIDs(charUUIds: NSArray, peripheral: CBPeripheral, service: CBService) {
        peripheral.discoverCharacteristics(charUUIds as? [CBUUID], for: service)
    }
    // MARK: Discover Descriptors
    func discoverDescriptorsByCharacteristic(peripheral: CBPeripheral, characteristic: CBCharacteristic) {
        peripheral.discoverDescriptors(for: characteristic)
    }
    func writeCharacteristicValue(peripheral: CBPeripheral, data: Data, char: CBCharacteristic, type: CBCharacteristicWriteType) {
        peripheral.writeValue(data, for: char, type: type)
    }
    // MARK: BLE Properties Reterival
    func readCharacteristicValue(peripheral: CBPeripheral, char: CBCharacteristic) {
        peripheral.readValue(for: char)
    }
    func setNotifyValue(peripheral: CBPeripheral, enabled: Bool, char: CBCharacteristic) {
       peripheral.setNotifyValue(enabled, for: char)
    }
    func writeDescriptorValue(peripheral: CBPeripheral, data: Data, descriptor: CBDescriptor) {
        peripheral.writeValue(data, for: descriptor)
    }
    func readDescriptorValue(peripheral: CBPeripheral, descriptor: CBDescriptor) {
        peripheral.readValue(for: descriptor)
    }
    // MARK: Read RSSI Value
    func readRSSI(peripheral: CBPeripheral) {
        peripheral.readRSSI()
    }
    func getConnectedPeripherals(serviceUUIds: NSArray) -> NSArray? {
        return self.centralManager?.retrieveConnectedPeripherals(withServices: (serviceUUIds as? [CBUUID])!) as NSArray?
    }
    func applyStopScanTimer() {
        stopScanTimer = Timer.scheduledTimer(timeInterval: TimeInterval(15.0), target: self, selector: #selector(stopScanningAfterInterval), userInfo: nil, repeats: false)
    }
}
