//
//  CBUUIDExtensions.swift
//  Thunderboard
//
//  Copyright © 2016 Silicon Labs. All rights reserved.
//

import CoreBluetooth

extension CBUUID {
    
    //MARK:- Service Identifiers
    
    static let BATTERY_SERVICE_UUID = CBUUID(string: "0x180f")
    
    static let IMMEDIATE_ALERT_SERVICE_UUID = CBUUID(string: "0x1802")
    
    static let CUSTOM_SERVICE = CBUUID(string: "0xfff0")
    
    // ---------------------------------------------------------------------------------------
    // MARK:- Characteristic Identifiers
    // ---------------------------------------------------------------------------------------
    
    static let CHARACTERISTIC_ALERT_LEVEL_UUID = CBUUID(string: "0x2a06");

    static let CHARACTERISTIC_BATTERY_LEVEL_UUID = CBUUID(string: "0x2a19");
    
    static let CHARACTERISTIC_CUSTOM_VERIFIED = CBUUID(string: "0xfff4");

    static let CHARACTERISTIC_KEY_PRESS_SRV_UUID = CBUUID(string: "0xffe0");
    
    static let CHARACTERISTIC_KEY_PRESS_UUID = CBUUID(string: "0xffe1");
    
    static let CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = CBUUID(string: "0x2902");

}
