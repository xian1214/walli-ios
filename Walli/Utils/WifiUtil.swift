//
//  WifiUtil.swift
//  Walli
//
//  Created by Xian Huang on 2/3/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation
import SystemConfiguration.CaptiveNetwork

public class WifiUtil{
    class func fetchCurrentSSIDInfo() -> String?{
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray?{
            for interface in interfaces{
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary?{
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break;
                }
            }
        }
        return ssid
    }


}
