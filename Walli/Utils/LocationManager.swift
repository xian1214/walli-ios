//
//  LocationManager.swift
//  Walli
//
//  Created by Xian Huang on 2/10/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Alamofire

private var sharedManager: LocationManager? = nil

class LocationManager: NSObject, CLLocationManagerDelegate {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation!
    var lastTime: Int = 0
    var emergencyCounter: Int = 0
    
    var locationupdates: Int = 0
    static func getSharedManager () -> LocationManager {
        if sharedManager == nil {
            sharedManager = LocationManager()
        }
        return sharedManager!
    }
    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        locationManager.requestWhenInUseAuthorization()
    }
    func startUpdatingLocation(){
        print("Starting location update...")
        //self.locationManager.startUpdatingHeading()
        self.locationManager.startUpdatingLocation()
    }
    func sendSMS(to: String, msg: String) {
        let accountSID = "AC84976a66f6ef65469e7e052fbfb8015f"
        let authToken = "804f635fa754b5460b1935015b0bf035"
        let url = "https://api.twilio.com/2010-04-01/Accounts/\(accountSID)/Messages"
        let parameters = ["From": "+17622631683", "To": to, "Body": msg]
        // +12342190239
        // +18565061291
        AF.request(url, method: .post, parameters: parameters)
            .authenticate(username: accountSID, password: authToken)
            .response { response in
                debugPrint(response)
        }
    }
    func saveLastLocation(){
        if self.currentLocation != nil{
            PrefsManager.shared.setLastLocation(loc: self.currentLocation)
            let latitude = self.currentLocation.coordinate.latitude
            let longitude = self.currentLocation.coordinate.longitude
            print("Location updated: \(latitude), \(longitude)")

            let emergencySMS = PrefsManager.shared.getEmergencySMS()
            if emergencySMS == 0 || emergencyCounter >= 5 {
                emergencyCounter = 0
                PrefsManager.shared.setEmergencySMS(value: 0)
                self.locationManager.stopUpdatingLocation()
                
                NotificationUtil.generateNotification(title: "Personal Safety Feature Setup Incomplete", body: "")
                return
            }
            let currentTime = Int(Date().timeIntervalSince1970 * 1000)
            if currentTime - lastTime >= 5*60*1000{
                var contactName = ""
                let myname = PrefsManager.shared.getContactMyName() == "" ? "xxx" : PrefsManager.shared.getContactMyName()
                let msg = "This is \(myname). I'm sending this emergency alert from my personal safety device. I need help and I’m located here: http://maps.google.com/maps?q=\(latitude),\(longitude)";
                var contactNumber1 = PrefsManager.shared.getContactNumber1()
                if contactNumber1 != "" {
                    contactName = PrefsManager.shared.getContactName1()
                    contactNumber1 = contactNumber1.replacingOccurrences(of: " ", with: "")
                    print("Sending to \(contactNumber1)")
//                    print(msg)
                    sendSMS(to: contactNumber1, msg: msg)
                }
                
                var contactNumber2 = PrefsManager.shared.getContactNumber2()
                if contactNumber2 != "" {
                    if contactName.isEmpty == false {
                        contactName = contactName + " & " + PrefsManager.shared.getContactName2()
                    }
                    else {
                        contactName = PrefsManager.shared.getContactName2()
                    }
                    contactNumber2 = contactNumber2.replacingOccurrences(of: " ", with: "")
                    print("Sending to \(contactNumber2)")
//                    print(msg)
                    sendSMS(to: contactNumber1, msg: msg)
                }
                if contactName.isEmpty == false {
                    NotificationUtil.generateNotification(title: "Personal Safety Feature Activated!", body: contactName + " will now have your live location for the next 30 minutes")
                }
                lastTime = currentTime
                emergencyCounter = emergencyCounter + 1
            }

        }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager?.stopUpdatingLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        self.currentLocation = locationArray.lastObject as? CLLocation
        self.saveLastLocation()
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var shouldIAllow = false
        switch status {
        case CLAuthorizationStatus.restricted:
            print("Location Manager Status Restricted")
            shouldIAllow = false
        case CLAuthorizationStatus.denied:
            print("Location Manager Status Denined")
            shouldIAllow = false
        case CLAuthorizationStatus.notDetermined:
            print("Location Manager Status Notdetermined")
            shouldIAllow = false
        default:
            print("Location Manager Status OK")
            shouldIAllow = true
        }
        if shouldIAllow == true{
            print("Location Manager started updating location")
            locationManager?.startMonitoringSignificantLocationChanges()
            locationManager?.allowsBackgroundLocationUpdates = true
            locationManager?.startUpdatingLocation()
        }
        appDelegate.cpvc?.refreshStatus()
    }
}
