//
//  Util.swift
//  Walli
//
//  Created by Xian Huang on 1/29/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import Foundation
import UIKit

class Util{
    /**
    * Show alert dialog
    *
    * @param title Dialog title
    * @param message Dialog message
    * @return void
    */
    static func showAlert(vc: UIViewController, _ title: String, _ message: String){
        let topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
        topWindow?.rootViewController = UIViewController()
        topWindow?.windowLevel = UIWindow.Level.alert + 1
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertController.Style.alert
        )

        alert.addAction(UIAlertAction(
            title: "OK",
            style: UIAlertAction.Style.default,
            handler: nil
        ))
        vc.present(alert, animated: true, completion: nil)
    }
    static func showToolTip(tip: String, view: UIView){
        /*
        let labelSize = tip.size(withAttributes: [.font: UIFont.systemFont(ofSize: 14)])
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: labelSize.height+20))
        label.numberOfLines = 0
        label.text = tip
        let popOver = DXPopover()
        popOver.show(at: view, withContentView: label)
         */
    }
    static func convertTimeStampToLiteral(date: Date) ->String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "MMM-dd h:mm a"

        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        return myString
    }
}
