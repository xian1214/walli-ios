//
//  NotificationUtil.swift
//  Walli
//
//  Created by Xian Huang on 2/3/20.
//  Copyright © 2020 Xian Huang. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth
import UserNotifications

public class NotificationUtil{
    enum NOTIFICATION_TYPE{
        case NOTIFICATION_ID_CONNECTED
        case NOTIFICATION_ID_DISCONNECTED
        case NOTIFICATION_ID_CARDOUT
        case NOTIFICATION_ID_FINDPHONE
        case NOTIFICATION_ID_BLUETOOTHOFF
    }

    class func sendNotification(title: String, body: String) {
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = .default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: "reminder", content: content, trigger: trigger)
        center.add(request) { (error) in
            if error != nil{
                print("Error = \(error?.localizedDescription ?? "error local notification")")
            }
        }
    }
    class func removePendingNotification(identifier: String){
        let center = UNUserNotificationCenter.current()
        center.removeDeliveredNotifications(withIdentifiers: [identifier])
    }
    class func generateNotification(type: NOTIFICATION_TYPE, walliR: WalliR?){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        var title = "Walli Notification"
        var body = ""
        var soundFileName = Constant.SOUND_NONE
        var filename = ""
        var identifier = "notifier_reminder"
        switch type {
        case .NOTIFICATION_ID_CONNECTED:
            body = walliR!.devicename + " Connected"
            soundFileName = Constant.SOUND_WALLI_CONNECT
        case .NOTIFICATION_ID_DISCONNECTED:
            body = "Hey, you forgot your " + walliR!.devicename
            soundFileName = Constant.SOUND_WALLI_DISCONNECT
        case .NOTIFICATION_ID_CARDOUT:
            if walliR?.devicetype == Constant.PRODUCT_WALLI{
                body = "Hey, did you forget your card?"
            }
            else if walliR?.devicetype == Constant.PRODUCT_WALLI_SLIM{
                body = "Hey, did you forget your slim?"
            }
            else if walliR?.devicetype == Constant.PRODUCT_WALLI_TRAVEL{
                body = "Hey, did you forget your passport?"
            }
            soundFileName = Constant.SOUND_CARD_OUT
            identifier = "notifier_cardout"
        case .NOTIFICATION_ID_FINDPHONE:
            body = "Looking for your phone?"
            soundFileName = Constant.SOUND_FIND_PHONE
        case .NOTIFICATION_ID_BLUETOOTHOFF:
            title = "Bluetooth is off"
            body = "Please turn on Bluetooth for Walli to work properly"
        default:
            body = ""
        }
        // separation alarm mode
        filename = "LOW-" + soundFileName + ".wav"
        
        content.title = title
        content.body = body
        
        var fireNotification = true
        
        if type == .NOTIFICATION_ID_BLUETOOTHOFF{
            content.sound = .default
        }
        else if type == .NOTIFICATION_ID_FINDPHONE || type == .NOTIFICATION_ID_CARDOUT{
            appDelegate.playBackgroundMusic(filename: filename)
            content.sound = .none
        }
        else{
            if PrefsManager.shared.getBackPacketMode(){
                if walliR?.alarmmode == WalliR.SEPARATION_ALARM_OFF{
                    fireNotification = false
                }
                print("Notification [BackPacketMode-Yes] \(filename)")
            }
            else{
                if walliR?.alarmmode == WalliR.SEPARATION_ALARM_OFF{
                    fireNotification = false
                }
                else if walliR?.alarmmode != WalliR.SEPARATION_ALARM_MUTE{
                    // if separation alarm mode is not mute
                    appDelegate.playBackgroundMusic(filename: filename)
                }
                print("Notification [BackPacketMode-No] \(filename)")

            }
            content.sound = .none
        }
        if fireNotification == false{
            return
        }
            
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        center.add(request) { (error) in
            if error != nil{
                print("Error = \(error?.localizedDescription ?? "error local notification")")
            }
        }
    }
    class func generateNotification(title: String, body: String){
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        let identifier = "notifier_reminder"
        
        content.sound = .default
        
        content.title = title
        content.body = body
        
            
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        center.add(request) { (error) in
            if error != nil{
                print("Error = \(error?.localizedDescription ?? "error local notification")")
            }
        }
    }

}
